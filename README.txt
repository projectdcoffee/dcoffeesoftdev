████──────████─████─███─███─███─███───
█──██─────█──█─█──█─█───█───█───█─────
█──██─███─█────█──█─███─███─███─███───
█──██─────█──█─█──█─█───█───█───█─────
████──────████─████─█───█───███─███───

# D-coffee 

	โปรแกรมร้านกาแฟสำหรับจัดการและซื้อขายและคำนวณยอดขาย

##
███─███─████─███─█─█─████─███
█───█───█──█──█──█─█─█──█─█──
███─███─████──█──█─█─████─███
█───█───█──█──█──█─█─█─█──█──
	-[Product]
	-[Material]
	-[User]
	-[Customer]
	-[Category]
	-[Employee]
	-[Receipt]
	-[POS]
	-[Bill]
	-[Store]
	-[CheckMaterial]
	-[SummarySalary]
	-[CheckInOut]
	-[Promotion]
	-[Supplier]
	-[Login , Logout]
	-[Member]
	-[CRUD AddExpenses]
	-[Chart Bar Report]

