/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class MatLowInStockReport {

    private int id;
    private String matName;
    private int matId;
    private int matQty;
    private Material material;

    public MatLowInStockReport(int id, String matName, int matId, int matQty) {
        this.id = id;
        this.matName = matName;
        this.matId = matId;
        this.matQty = matQty;
    }

    public MatLowInStockReport(String matName, int matId, int matQty) {
        this.id = -1;
        this.matName = matName;
        this.matId = matId;
        this.matQty = matQty;
    }

    public MatLowInStockReport() {
        this(1, "", 0, 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMatName() {
        return matName;
    }

    public void setMatName(String matName) {
        this.matName = matName;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    public int getMatQty() {
        return matQty;
    }

    public void setMatQty(int matQty) {
        this.matQty = matQty;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "MatLowInStockReport{" + "id=" + id + ", matName=" + matName + ", matId=" + matId + ", matQty=" + matQty + ", material=" + material + '}';
    }
    
    
    
        public static MatLowInStockReport fromRS(ResultSet rs) {
        MatLowInStockReport matLowInStockReport1 = new MatLowInStockReport();
        try {
            matLowInStockReport1.setMatId(rs.getInt("mat_id"));
            matLowInStockReport1.setMatName(rs.getString("mat_name"));
            matLowInStockReport1.setMatQty(rs.getInt("mat_qty"));
            
            // Propulation
            MaterialDao materialDao = new MaterialDao();            
            Material material = materialDao.get(matLowInStockReport1.getMatId());
            matLowInStockReport1.setMaterial(material);

        } catch (SQLException ex) {
            Logger.getLogger(MatLowInStockReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return matLowInStockReport1;
    }
    
    

}
