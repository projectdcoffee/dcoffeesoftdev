/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author slmfr
 */
public class CheckInOut {

    private int id;
    private Date date;
    private Timestamp out;
    private Timestamp in;
    private float totalHour;
    private int ssId;
    private int employeeId;
    private Employee employee;

    public CheckInOut(int id, Date date, Timestamp in, Timestamp out, float totalHour, int ssId, int employeeId) {
        this.id = id;
        this.date = date;
        this.in = in;
        this.out = out;
        this.totalHour = totalHour;
        this.ssId = ssId;
        this.employeeId = employeeId;
    }

    public CheckInOut(Date date, Timestamp in, Timestamp out, float totalHour, int employeeId) {
        this.id = -1;
        this.date = date;
        this.in = in;
        this.out = out;
        this.totalHour = totalHour;
        this.ssId = -1;
        this.employeeId = employeeId;
    }

    public CheckInOut() {
        this.id = -1;
        this.date = null;
        this.in = null;
        this.out = null;
        this.totalHour = 0.0f;
        this.ssId = -1;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Timestamp getOut() {
        return out;
    }

    public void setOut(Timestamp out) {
        this.out = out;
    }

    public Timestamp getIn() {
        return in;
    }

    public void setIn(Timestamp in) {
        this.in = in;
    }

    public float getTotalHour() {
        return totalHour;
    }

    public void setTotalHour(float totalHour) {
        this.totalHour = totalHour;
    }

    public int getSsId() {
        return ssId;
    }

    public void setSsId(int ssId) {
        this.ssId = ssId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", out=" + out + ", in=" + in + ", totalHour=" + totalHour + ", ssId=" + ssId + ", employeeId=" + employeeId + ", employee=" + employee + '}';
    }

    public void calculateTotaHour() {
        Duration duration = Duration.between(this.in.toInstant(), this.out.toInstant());
        long hours = duration.toHours();
        long minutes = duration.toMinutesPart();
        String srtfloat = hours + "." + minutes;
        float floatValue = Float.parseFloat(srtfloat);
        this.totalHour = floatValue;
    }

    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("cio_id"));
            checkInOut.setDate(rs.getDate("cio_date"));
//            System.out.println(rs.getTimestamp("cio_in"));
            checkInOut.setIn(rs.getTimestamp("cio_in"));
            checkInOut.setOut(rs.getTimestamp("cio_out"));
            checkInOut.setTotalHour(rs.getFloat("cio_total_hour"));
            checkInOut.setSsId(rs.getInt("ss_id"));
            checkInOut.setEmployeeId(rs.getInt("employee_id"));

            // Propulation
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkInOut.getEmployeeId());
            checkInOut.setEmployee(employee);

        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    }

}
