/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class CheckMaterial {

    private int id;
    private int employeeId;
    private Date createdDate;
    private Employee employee;
    private ArrayList<CheckMaterialDetail> cmd = new ArrayList<CheckMaterialDetail>();

    public CheckMaterial(int id, int employeeId, Date createdDate) {
        this.id = id;
        this.employeeId = employeeId;
        this.createdDate = createdDate;
    }

    public CheckMaterial(int employeeId, Date createdDate) {
        this.id = -1;
        this.employeeId = employeeId;
        this.createdDate = createdDate;
    }

    public CheckMaterial() {
        this.id = -1;
        this.employeeId = 0;
        this.createdDate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public ArrayList<CheckMaterialDetail> getCmd() {
        return cmd;
    }

    public void setCmd(ArrayList<CheckMaterialDetail> cmd) {
        this.cmd = cmd;
    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "id=" + id + ", employeeId=" + employeeId + ", createdDate=" + createdDate + ", employee=" + employee + '}';
    }

    public void addCMD(CheckMaterialDetail checkMD) {
        cmd.add(checkMD);
    }

    public void addCMD(Material material) {
        String materialName = material.getName();
        boolean alreadyExists = false;

        for (CheckMaterialDetail cmdDetail : cmd) {
            if (cmdDetail.getCmdName().equals(materialName)) {
                alreadyExists = true;
                break;
            }
        }

        if (!alreadyExists) {
            CheckMaterialDetail checkMatDetail = new CheckMaterialDetail(materialName, material.getQty(), 0, 0, material.getId());
            cmd.add(checkMatDetail);
        }
    }

    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial checkMaterial = new CheckMaterial();
        try {
            checkMaterial.setId(rs.getInt("check_mat_id"));
            checkMaterial.setEmployeeId(rs.getInt("employee_id"));
            checkMaterial.setCreatedDate(rs.getTimestamp("check_mat_datetime"));

            //populate fk
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkMaterial.getEmployeeId());
            checkMaterial.setEmployee(employee);

        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMaterial;
    }
}
