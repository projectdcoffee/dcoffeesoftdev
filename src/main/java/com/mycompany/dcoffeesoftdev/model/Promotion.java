/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fkais
 */
public class Promotion {
    private int id;
    private String name;
    private String codePro;
    private int discount;
    private Date lastdate;

    public Promotion(int id, String name, String codePro, int discount, Date lastdate) {
        this.id = id;
        this.name = name;
        this.codePro = codePro;
        this.discount = discount;
        this.lastdate = lastdate;
    }

    public Promotion(String name, String codePro, int discount, Date lastdate) {
        this.id = -1;
        this.name = name;
        this.codePro = codePro;
        this.discount = discount;
        this.lastdate = lastdate;
    }

    public Promotion() {
        this.id = -1;
        this.name = "";
        this.codePro = "";
        this.discount = 0;
        this.lastdate = null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCodePro() {
        return codePro;
    }

    public Date getLastdate() {
        return lastdate;
    }

    public int getDiscount() {
        return discount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCodePro(String codePro) {
        this.codePro = codePro;
    }

    public void setLastdate(Date lastdate) {
        this.lastdate = lastdate;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", codePro=" + codePro + ", discount=" + discount + ", lastdate=" + lastdate + '}';
    }

    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("pmt_id"));
            promotion.setName(rs.getString("pmt_name"));
            promotion.setCodePro(rs.getString("pmt_codepro"));
            promotion.setDiscount(rs.getInt("pmt_discount"));
            promotion.setLastdate(rs.getDate("pmt_datetime"));
        } catch(SQLException ex) {
             Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
}
