/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Love_
 */
public class ReceiptDetail {

    private int id;
    private int proId;
    private String proName;
    private int qty;
    private float proPrice;
    private float totalPrice;
    private int discount;
    private int receiptId;
    private String size;
    private String type;

    public ReceiptDetail(int id, int proId, String proName, int qty, float proPrice, float totalPrice, int discount, int receiptId , String size , String type) {
        this.id = id;
        this.proId = proId;
        this.proName = proName;
        this.qty = qty;
        this.proPrice = proPrice;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.receiptId = receiptId;
        this.size = size;
        this.type = type;
    }

    public ReceiptDetail(int proId, String proName, int qty, float proPrice, float totalPrice, int discount, int receiptId, String size , String type) {
        this.id = -1;
        this.proId = proId;
        this.proName = proName;
        this.qty = qty;
        this.proPrice = proPrice;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.receiptId = receiptId;
        this.size = size;
        this.type = type;
    }

    public ReceiptDetail() {
        this.id = -1;
        this.proId = 0;
        this.proName = "";
        this.qty = 0;
        this.proPrice = 0;
        this.totalPrice = 0;
        this.discount = 0;
        this.receiptId = 0;
        this.size = "";
        this.type = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProId() {
        return proId;
    }

    public void setProId(int proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getProPrice() {
        return proPrice;
    }

    public void setProPrice(float proPrice) {
        this.proPrice = proPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

    public String getSize() {
        return size;
    }

    public String getType() {
        return type;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", proId=" + proId + ", proName=" + proName + ", qty=" + qty + ", proPrice=" + proPrice + ", totalPrice=" + totalPrice + ", discount=" + discount + ", receiptId=" + receiptId + ", size=" + size + ", type=" + type + '}';
    }
    


    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        try {
            receiptDetail.setId(rs.getInt("rcd_id"));
            receiptDetail.setProId(rs.getInt("product_id"));
            receiptDetail.setProName(rs.getString("rcd_name"));
            receiptDetail.setQty(rs.getInt("rcd_qty"));
            receiptDetail.setProPrice(rs.getFloat("rcd_price"));
            receiptDetail.setTotalPrice(rs.getFloat("rcd_total"));
            receiptDetail.setDiscount(rs.getInt("rcd_discount"));
            receiptDetail.setReceiptId(rs.getInt("rec_id"));
            receiptDetail.setProId(rs.getInt("product_id"));
            receiptDetail.setType(rs.getString("rcd_type"));
            receiptDetail.setSize(rs.getString("rcd_size"));
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetail;
    }
}
