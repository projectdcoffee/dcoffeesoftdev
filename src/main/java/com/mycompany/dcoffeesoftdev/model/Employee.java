/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Employee {

    private int id;
    private String fname;
    private String lname;
    private float WorkHourlyWage;
    private String tel;
    private String address;
    private String email;
    private String position;
    private int userId;
    private User user;

    public Employee(int id, String fname, String lname, float WorkHourlyWage, String tel, String address, String email, String position, int userId) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.WorkHourlyWage = WorkHourlyWage;
        this.tel = tel;
        this.address = address;
        this.email = email;
        this.position = position;
        this.userId = userId;
    }

    public Employee(String fname, String lname, float WorkHourlyWage, String tel, String address, String email, String position, int userId) {
        this.id = -1;
        this.fname = fname;
        this.lname = lname;
        this.WorkHourlyWage = 0;
        this.tel = tel;
        this.address = address;
        this.email = email;
        this.position = position;
        this.userId = userId;
    }

    public Employee() {
        this.id = -1;
        this.fname = "";
        this.lname = "";
        this.WorkHourlyWage = 0;
        this.tel = "";
        this.address = "";
        this.email = "";
        this.position = "";
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public float getWorkHourlyWage() {
        return WorkHourlyWage;
    }

    public void setWorkHourlyWage(float WorkHourlyWage) {
        this.WorkHourlyWage = WorkHourlyWage;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + fname + ", lastname=" + lname + ", WorkHourlyWage=" + WorkHourlyWage + ", tel=" + tel + ", address=" + address + ", email=" + email + ", position=" + position + ", userId=" + userId + ", user=" + user + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setFname(rs.getString("employee_fname"));
            employee.setLname(rs.getString("employee_lname"));
            employee.setWorkHourlyWage(rs.getFloat("employee_work_hourly_wage"));
            employee.setTel(rs.getString("employee_tel"));
            employee.setAddress(rs.getString("employee_address"));
            employee.setEmail(rs.getString("employee_email"));
            employee.setPosition(rs.getString("employee_position"));
            employee.setUserId(rs.getInt("user_id"));

//            UserDao userDao = new UserDao();
//            User user = userDao.get(employee.getUserId());
//            employee.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
