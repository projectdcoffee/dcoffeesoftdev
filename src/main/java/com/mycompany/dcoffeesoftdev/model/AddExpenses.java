/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fkais
 */
public class AddExpenses {

    private int id;
    private Date datetime;
    private float total;
    private ArrayList<AddExpensesDetail> addExpensesDetails = new ArrayList<AddExpensesDetail>();

    public AddExpenses(int id, Date datetime, float total) {
        this.id = id;
        this.datetime = datetime;
        this.total = total;
    }

    public AddExpenses(Date datetime, float total) {
        this.id = -1;
        this.datetime = datetime;
        this.total = total;
    }

    public AddExpenses() {
        this.id = -1;
        this.datetime = null;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public Date getDateTime() {
        return datetime;
    }

    public float getTotal() {
        return total;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateTime(Date datetime) {
        this.datetime = datetime;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    public ArrayList<AddExpensesDetail> getAddExpensesDetail() {
        return addExpensesDetails;
    }
    
    public void setAddExpensesDetail(ArrayList addExpensesDetails) {
        this.addExpensesDetails = addExpensesDetails;
        
    }
    
    public void calculateTotal() {
        float total = 0.0f;
        for (AddExpensesDetail aed : addExpensesDetails) {
            total += aed.getTotal();

        }
        this.total = total;
    }
    
    public void addAddExpensesDetail(AddExpensesDetail addExpensesDetail) {
        addExpensesDetails.add(addExpensesDetail);
        calculateTotal();
    }

    @Override
    public String toString() {
        return "AddExpenses{" + "id=" + id + ", datetime=" + datetime + ", total=" + total + ", addExpensesDetails=" + addExpensesDetails + '}';
    }



    public static AddExpenses fromRs(ResultSet rs) {
        AddExpenses addExpenses = new AddExpenses();
        try {
            addExpenses.setId(rs.getInt("ae_id"));
            addExpenses.setDateTime(rs.getTimestamp("ae_datetime"));
            addExpenses.setTotal(rs.getFloat("ae_total"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return addExpenses;
    }
}
