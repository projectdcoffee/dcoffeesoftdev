/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fkais
 */
public class AddExpensesDetail {
    private int id;
    private String name;
    private float total;
    private int addExpensesId;

    public AddExpensesDetail(int id, String name, float total, int addExpensesId) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.addExpensesId = addExpensesId;
    }

    public AddExpensesDetail(String name, float total, int addExpensesId) {
        this.id = -1;
        this.name = name;
        this.total = total;
        this.addExpensesId = addExpensesId;
    }

    public AddExpensesDetail() {
        this.id = -1;
        this.name = "";
        this.total = 0;
        this.addExpensesId = 0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getTotal() {
        return total;
    }

    public int getAddExpensesId() {
        return addExpensesId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public void setAddExpensesId(int addExpensesId) {
        this.addExpensesId = addExpensesId;
    }

    @Override
    public String toString() {
        return "AddExpensesDetail{" + "id=" + id + ", name=" + name + ", total=" + total + ", addExpensesId=" + addExpensesId + '}';
    }
    
    public static AddExpensesDetail fromRS(ResultSet rs) {
        AddExpensesDetail addExpensesDetail = new AddExpensesDetail();
        try {
            addExpensesDetail.setId(rs.getInt("aed_id"));
            addExpensesDetail.setName(rs.getString("aed_name"));
            addExpensesDetail.setTotal(rs.getFloat("aed_total"));
            addExpensesDetail.setAddExpensesId(rs.getInt("ae_id"));
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return addExpensesDetail;
    }
}
