/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Love_
 */
public class BillDetail {

    private int id;
    private int billId;
    private int materialId;
    private String name;
    private float price;
    private int amout;
    private float total;
    private Material material;

    public BillDetail(int id, int billId, int materialId, String name, float price, int amout, float total) {
        this.id = id;
        this.billId = billId;
        this.materialId = materialId;
        this.name = name;
        this.price = price;
        this.amout = amout;
        this.total = total;
    }

    public BillDetail(int billId, int materialId, String name, float price, int amout, float total) {
        this.id = -1;
        this.billId = billId;
        this.materialId = materialId;
        this.name = name;
        this.price = price;
        this.amout = amout;
        this.total = total;
    }

    public BillDetail() {
        this.id = -1;
        this.billId = 0;
        this.materialId = 0;
        this.name = "";
        this.price = 0;
        this.amout = 0;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getAmout() {
        return amout;
    }

    public void setAmout(int amout) {
        this.amout = amout;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", billId=" + billId + ", materialId=" + materialId + ", name=" + name + ", price=" + price + ", amout=" + amout + ", total=" + total + ", material=" + material + '}';
    }



    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billDetail = new BillDetail();
        try {
            billDetail.setId(rs.getInt("bill_detail_id"));
            billDetail.setBillId(rs.getInt("bill_id"));
            billDetail.setMaterialId(rs.getInt("mat_id"));
            billDetail.setName(rs.getString("bill_detail_name"));
            billDetail.setPrice(rs.getFloat("bill_detail_price"));
            billDetail.setAmout(rs.getInt("bill_detail_amount"));
            billDetail.setTotal(rs.getFloat("bill_detail_total"));

            MaterialDao materialDao = new MaterialDao();
            Material material = materialDao.get(billDetail.materialId);
            billDetail.setMaterial(material);

        } catch (SQLException ex) {
            Logger.getLogger(BillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billDetail;
    }

}
