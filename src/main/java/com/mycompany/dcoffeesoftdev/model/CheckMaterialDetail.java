/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.CheckMaterialDao;
import com.mycompany.dcoffeesoftdev.dao.CheckMaterialDetailDao;
import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Love_
 */
public class CheckMaterialDetail {

    private int id;
    private String cmdName;
    private int cmdQtyLast;
    private int cmdQtyRemain;
    private int cmdQtyExpired;
    private int matId;
    private int checkMatId;
    private Material material;
    private CheckMaterial checkMaterial;

    public CheckMaterialDetail(int id, String cmdName, int cmdQtyLast, int cmdQtyRemain, int cmdQtyExpired, int matId, int checkMatId) {
        this.id = id;
        this.cmdName = cmdName;
        this.cmdQtyLast = cmdQtyLast;
        this.cmdQtyRemain = cmdQtyRemain;
        this.cmdQtyExpired = cmdQtyExpired;
        this.matId = matId;
        this.checkMatId = checkMatId;
    }

    public CheckMaterialDetail(String cmdName, int cmdQtyLast, int cmdQtyRemain, int cmdQtyExpired, int matId, int checkMatId) {
        this.id = -1;
        this.cmdName = cmdName;
        this.cmdQtyLast = cmdQtyLast;
        this.cmdQtyRemain = cmdQtyRemain;
        this.cmdQtyExpired = cmdQtyExpired;
        this.matId = matId;
        this.checkMatId = checkMatId;
    }

    public CheckMaterialDetail(String cmdName, int cmdQtyLast, int cmdQtyRemain, int cmdQtyExpired, int matId) {
        this.id = -1;
        this.cmdName = cmdName;
        this.cmdQtyLast = cmdQtyLast;
        this.cmdQtyRemain = cmdQtyRemain;
        this.cmdQtyExpired = cmdQtyExpired;
        this.matId = matId;

    }

    public CheckMaterialDetail() {
        this.id = -1;
        this.cmdName = "";
        this.cmdQtyLast = 0;
        this.cmdQtyRemain = 0;
        this.cmdQtyExpired = 0;
        this.matId = 0;
        this.checkMatId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCmdName() {
        return cmdName;
    }

    public void setCmdName(String cmdName) {
        this.cmdName = cmdName;
    }

    public int getCmdQtyLast() {
        return cmdQtyLast;
    }

    public void setCmdQtyLast(int cmdQtyLast) {
        this.cmdQtyLast = cmdQtyLast;
    }

    public int getCmdQtyRemain() {
        return cmdQtyRemain;
    }

    public void setCmdQtyRemain(int cmdQtyRemain) {
        this.cmdQtyRemain = cmdQtyRemain;
    }

    public int getCmdQtyExpired() {
        return cmdQtyExpired;
    }

    public void setCmdQtyExpired(int cmdQtyExpired) {
        this.cmdQtyExpired = cmdQtyExpired;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
        
        MaterialDao matDao = new MaterialDao();
        Material material = matDao.get(matId);
        this.material = material;
    }

    public int getCheckMatId() {
        return checkMatId;
    }

    public void setCheckMatId(int checkMatId) {
        this.checkMatId = checkMatId;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
        this.matId = material.getId();
    }

    public CheckMaterial getCheckMaterial() {
        return checkMaterial;
    }

    public void setCheckMaterial(CheckMaterial checkMaterial) {
        this.checkMaterial = checkMaterial;
        this.checkMatId = checkMaterial.getId();
    }

    @Override
    public String toString() {
        return "CheckMaterialDetail{" + "id=" + id + ", cmdName=" + cmdName + ", cmdQtyLast=" + cmdQtyLast + ", cmdQtyRemain=" + cmdQtyRemain + ", cmdQtyExpired=" + cmdQtyExpired + ", matId=" + matId + ", checkMatId=" + checkMatId + ", material=" + material + ", checkMaterial=" + checkMaterial + '}';
    }

    public static CheckMaterialDetail fromRS(ResultSet rs) {
        CheckMaterialDetail checkMaterialDetail = new CheckMaterialDetail();
        try {
            checkMaterialDetail.setId(rs.getInt("cmd_id"));
            checkMaterialDetail.setCmdName(rs.getString("cmd_name"));
            checkMaterialDetail.setCmdQtyLast(rs.getInt("cmd_qty_last"));
            checkMaterialDetail.setCmdQtyRemain(rs.getInt("cmd_qty_remain"));
            checkMaterialDetail.setCmdQtyExpired(rs.getInt("cmd_qty_expired"));
            checkMaterialDetail.setCheckMatId(rs.getInt("check_mat_id"));
            checkMaterialDetail.setMatId(rs.getInt("mat_id"));

            //populate fk
            MaterialDao materialDao = new MaterialDao();
            Material material = materialDao.get(checkMaterialDetail.getMatId());
            checkMaterialDetail.setMaterial(material);

            CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
            CheckMaterial checkMaterial = checkMaterialDao.get(checkMaterialDetail.getCheckMatId());
            checkMaterialDetail.setCheckMaterial(checkMaterial);

        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMaterialDetail;
    }
}
