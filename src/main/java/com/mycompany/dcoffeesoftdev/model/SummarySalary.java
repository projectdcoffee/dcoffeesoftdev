/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Love_
 */
public class SummarySalary {

    private int id;
    private Date date;
    private float workhour;
    private float salary;
    private int emId;
    private Employee employee;

    public SummarySalary(int id, Date date, float workhour, float salary, int emId) {
        this.id = id;
        this.date = null;
        this.workhour = workhour;
        this.salary = salary;
        this.emId = emId;
    }

    public SummarySalary(Date date, float workhour, float salary, int emId) {
        this.id = -1;
        this.date = null;
        this.workhour = workhour;
        this.salary = salary;
        this.emId = -1;
    }

    public SummarySalary() {
        this.id = -1;
        this.date = null;
        this.workhour = 0;
        this.salary = 0.0f;
        this.emId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getWorkhour() {
        return workhour;
    }

    public void setWorkhour(float workhour) {
        this.workhour = workhour;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getEmId() {
        return emId;
    }

    public void setEmId(int emId) {
        this.emId = emId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "SummarySalary{" + "id=" + id + ", date=" + date + ", workhour=" + workhour + ", salary=" + salary + ", emId=" + emId + '}';
    }

    public static SummarySalary fromRS(ResultSet rs) {
        SummarySalary summarysalary = new SummarySalary();
        try {
            summarysalary.setId(rs.getInt("ss_id"));
            summarysalary.setDate(rs.getDate("ss_date"));
            summarysalary.setWorkhour(rs.getFloat("ss_work_hour"));
            summarysalary.setSalary(rs.getFloat("ss_salary"));
            summarysalary.setEmId(rs.getInt("employee_id"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(summarysalary.getEmId());
            summarysalary.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return summarysalary;
    }
}
