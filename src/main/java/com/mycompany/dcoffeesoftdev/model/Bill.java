/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.BillDetailDao;
import com.mycompany.dcoffeesoftdev.dao.EmployeeDao;
import com.mycompany.dcoffeesoftdev.dao.SupplierDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class Bill {

    private int id;
    private int employeeId;
    private int supplierId;
    private Date datetime;
    private float total;
    private float buy;
    private float change;
    private Employee employee;
    private Supplier supplier;
    private ArrayList<BillDetail> billDetails  = new ArrayList<BillDetail>();

    public Bill(int id, int employeeId, int supplierId, Date datetime, int total, int buy, int change) {
        this.id = id;
        this.employeeId = employeeId;
        this.supplierId = supplierId;
        this.datetime = datetime;
        this.total = total;
        this.buy = buy;
        this.change = change;
        
    }

    public Bill(int employeeId, int supplierId, Date datetime, int total, int buy, int change) {
        this.id = -1;
        this.employeeId = employeeId;
        this.supplierId = employeeId;
        this.datetime = datetime;
        this.total = total;
        this.buy = buy;
        this.change = change;
       
    }

    public Bill() {
        this.id = -1;
        this.employeeId = 0;
        this.supplierId = 0;
        this.datetime = null;
        this.total = 0;
        this.buy = 0;
        this.change = 0;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        // this.employeeId = employee.getId();
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
        // this.supplierId = supplier.getId();
    }

    public Date getDateTime() {
        return datetime;
    }

    public void setDateTime(Date datetime) {
        this.datetime = datetime;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getBuy() {
        return buy;
    }

    public void setBuy(float buy) {
        this.buy = buy;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public ArrayList<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(ArrayList billDetails) {
        this.billDetails = billDetails;
    }

    public void addBillDetail(BillDetail billDetail) {
       billDetails.add(billDetail);
        calculateTotal();
    }

    public void delBillDetail(BillDetail billDetail) {
        billDetails.remove(billDetail);
        calculateTotal();
    }

    private void calculateTotal() {
        float total = 0.00f;
        for (BillDetail bd : billDetails) {
            total += bd.getAmout() * bd.getPrice();
        }
        this.total = total;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", employeeId=" + employeeId + ", supplierId=" + supplierId + ", datetime=" + datetime + ", total=" + total + ", buy=" + buy + ", change=" + change + ", employee=" + employee + ", supplier=" + supplier + ", billDetails=" + billDetails + '}';
    }



    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setEmployeeId(rs.getInt("employee_id"));
            bill.setSupplierId(rs.getInt("supplier_id"));
            bill.setDateTime(rs.getTimestamp("bill_datetime"));
            bill.setTotal(rs.getFloat("bill_total"));
            bill.setBuy(rs.getFloat("bill_buy"));
            bill.setChange(rs.getFloat("bill_change"));

            EmployeeDao employeeDao = new EmployeeDao();
            SupplierDao supplierDao = new SupplierDao();
            Employee employee = employeeDao.get(bill.getEmployeeId());
            Supplier supplier = supplierDao.get(bill.getSupplierId());
            BillDetailDao billDetailDao = new BillDetailDao();
            List<BillDetail> list  = billDetailDao.getByBill(bill.getId());
            for(BillDetail bd : list){
                bill.addBillDetail(bd);
            }
            bill.setEmployee(employee);
            bill.setSupplier(supplier);

        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
