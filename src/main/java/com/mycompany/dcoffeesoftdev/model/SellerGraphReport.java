/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;
import com.mycompany.dcoffeesoftdev.dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class SellerGraphReport {

    private int id;
    private int sumTotal;
    private String month;
    private Receipt receipt;

    public SellerGraphReport(int id, int sumTotal, String month) {
        this.id = id;
        this.sumTotal = sumTotal;
        this.month = month;
    }

        public SellerGraphReport(int sumTotal, String month) {
        this.id = -1;
        this.sumTotal = sumTotal;
        this.month = month;
    }

    public SellerGraphReport() {
        this(1, 0, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSumTotal() {
        return sumTotal;
    }

    public void setSumTotal(int sumTotal) {
        this.sumTotal = sumTotal;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    @Override
    public String toString() {
        return "SellerGraphReport{" + "id=" + id + ", sumTotal=" + sumTotal + ", month=" + month + ", receipt=" + receipt + '}';
    }

    
    
    public static SellerGraphReport fromRS(ResultSet rs) {
        SellerGraphReport sellerGraphReport = new SellerGraphReport();
        try {
            sellerGraphReport.setSumTotal(rs.getInt("rec_total"));
            sellerGraphReport.setMonth(rs.getString("month"));

        } catch (SQLException ex) {
            Logger.getLogger(SellerGraphReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return sellerGraphReport;
    }

}
