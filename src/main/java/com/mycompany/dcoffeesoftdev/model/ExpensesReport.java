/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class ExpensesReport {

    private int id;
    private String aeDatetime;
    private int aeTotal;
    private AddExpenses addExpenses;

    public ExpensesReport(int id, String aeDatetime, int aeTotal) {
        this.id = id;
        this.aeDatetime = aeDatetime;
        this.aeTotal = aeTotal;
    }

    public ExpensesReport(String aeDatetime, int aeTotal) {
        this.id = -1;
        this.aeDatetime = aeDatetime;
        this.aeTotal = aeTotal;
    }

    public ExpensesReport() {
        this(1, "", 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAeDatetime() {
        return aeDatetime;
    }

    public void setAeDatetime(String aeDatetime) {
        this.aeDatetime = aeDatetime;
    }

    public int getAeTotal() {
        return aeTotal;
    }

    public void setAeTotal(int aeTotal) {
        this.aeTotal = aeTotal;
    }

    public AddExpenses getAddExpenses() {
        return addExpenses;
    }

    public void setAddExpenses(AddExpenses addExpenses) {
        this.addExpenses = addExpenses;
    }

    @Override
    public String toString() {
        return "ExpensesReport{" + "id=" + id + ", aeDatetime=" + aeDatetime + ", aeTotal=" + aeTotal + ", addExpenses=" + addExpenses + '}';
    }

   

    public static ExpensesReport fromRS(ResultSet rs) {
        ExpensesReport expensesReport = new ExpensesReport();
        try {
            expensesReport.setAeDatetime(rs.getString("ae_date"));
            expensesReport.setAeTotal(rs.getInt("ae_total"));

        } catch (SQLException ex) {
            Logger.getLogger(ExpensesReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expensesReport;
    }

}
