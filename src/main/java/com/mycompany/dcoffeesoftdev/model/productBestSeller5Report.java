/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class ProductBestSeller5Report {

    private int id;
    private String productName;
    private int productQty;
    private int productId;
    private Product product;

    public ProductBestSeller5Report(int id, String productName, int productQty, int productId) {
        this.id = id;
        this.productName = productName;
        this.productQty = productQty;
        this.productId = productId;
    }

    public ProductBestSeller5Report(String productName, int productQty, int productId) {
        this.id = -1;
        this.productName = productName;
        this.productQty = productQty;
        this.productId = productId;
    }

    public ProductBestSeller5Report() {
        this(1, " ", 0, 1);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ProductBestSeller5Report{" + "id=" + id + ", productName=" + productName + ", productQty=" + productQty + ", productId=" + productId + ", product=" + product + '}';
    }

    public static ProductBestSeller5Report fromRS(ResultSet rs) {
        ProductBestSeller5Report productBestSeller5Report1 = new ProductBestSeller5Report();
        try {
            productBestSeller5Report1.setProductId(rs.getInt("product_id"));
            productBestSeller5Report1.setProductName(rs.getString("product_name"));
            productBestSeller5Report1.setProductQty(rs.getInt("rcd_qty"));

        } catch (SQLException ex) {
            Logger.getLogger(ProductBestSeller5Report.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return productBestSeller5Report1;
    }

}
