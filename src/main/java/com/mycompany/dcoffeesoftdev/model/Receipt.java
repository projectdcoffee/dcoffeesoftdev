/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import com.mycompany.dcoffeesoftdev.dao.CustomerDao;
import com.mycompany.dcoffeesoftdev.dao.EmployeeDao;
import com.mycompany.dcoffeesoftdev.dao.PromotionDao;
import com.mycompany.dcoffeesoftdev.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fkais
 */
public class Receipt {

    private int id;
    private Date createdDate;
    private float total;
    private int totalQty;
    private float discount;
    private float cash;
    private float change;
    private String paymentType;
    private int storeId;
    private int employeeId;
    private int customerId;
    private int promotionId;
    private Store store;
    private Employee employee;
    private Customer customer;
    private Promotion promotion;
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();

    public Receipt(int id, Date createdDate, float total, int totalQty, float discount, float cash, float change, String paymentType, int storeId, int employeeId, int customerId, int promotionId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.totalQty = totalQty;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.paymentType = paymentType;
        this.storeId = storeId;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
    }

    public Receipt(Date createdDate, float total, int totalQty, float discount, float cash, float change, String paymentType, int storeId, int employeeId, int customerId, int promotionId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.totalQty = totalQty;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.paymentType = paymentType;
        this.storeId = storeId;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
    }

    public Receipt(float total, int totalQty, float discount, float cash, float change, String paymentType, int storeId, int employeeId, int customerId, int promotionId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.totalQty = totalQty;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.paymentType = paymentType;
        this.storeId = storeId;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
    }

    public Receipt(int totalQty, float discount, float cash, float change, String paymentType, int storeId, int employeeId, int customerId, int promotionId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.totalQty = totalQty;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        this.paymentType = paymentType;
        this.storeId = storeId;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
    }

    public Receipt() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.totalQty = 0;
        this.discount = 0;
        this.cash = 0;
        this.change = 0;
        this.paymentType = "";
        this.storeId = 0;
        this.employeeId = 0;
        this.customerId = 0;
        this.promotionId =0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
        this.storeId = store.getId();
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Customer getCustomer() {
        return customer;
    }
    

    public void setCustomer(Customer customer) {
        this.customer = customer;
//        this.customerId = customer.getId();
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", totalQty=" + totalQty + ", discount=" + discount + ", cash=" + cash + ", change=" + change + ", paymentType=" + paymentType + ", storeId=" + storeId + ", employeeId=" + employeeId + ", customerId=" + customerId + ", promotionId=" + promotionId + ", store=" + store + ", employee=" + employee + ", customer=" + customer + ", promotion=" + promotion + ", receiptDetails=" + receiptDetails + '}';
    }

    public void addRecieptDetail(ReceiptDetail recieptDetail) {
        receiptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addRecieptDetail(Product product, int qty,String size , String type) {
//        System.out.println( product+" "+size + " " + type);
        for (ReceiptDetail rd : receiptDetails) {
            if (rd.getProId()== product.getId() && size.equals(rd.getSize()) && type.equals(rd.getType())) {
                int currentQty = rd.getQty();
                int newQty = currentQty + qty;
                rd.setQty(newQty);
                rd.setTotalPrice(rd.getProPrice()* newQty);
                rd.setSize(size);
                rd.setType(type);
                calculateTotal();
                return;
            }
        }
        ReceiptDetail rd = new ReceiptDetail(product.getId(), product.getName(),qty, product.getPrice(), product.getPrice() * qty, 0, -1, size, type);
        System.out.println(rd);
        receiptDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(ReceiptDetail recieptDetail) {
        receiptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (ReceiptDetail rd : receiptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();

        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt reciept = new Receipt();
        try {
            reciept.setId(rs.getInt("rec_id"));
            reciept.setCreatedDate(rs.getTimestamp("rec_datetime"));
            reciept.setTotal(rs.getFloat("rec_total"));
            reciept.setTotalQty(rs.getInt("rec_totalQty"));
            reciept.setDiscount(rs.getFloat("rec_discount"));
            reciept.setCash(rs.getFloat("rec_cash"));
            reciept.setChange(rs.getFloat("rec_change"));
            reciept.setPaymentType(rs.getString("rec_payment_type"));
            reciept.setStoreId(rs.getInt("store_id"));
            reciept.setEmployeeId(rs.getInt("employee_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            reciept.setPromotionId(rs.getInt("pmt_id"));

            // Propulation
            CustomerDao customerDao = new CustomerDao();
            EmployeeDao employeeDao = new EmployeeDao();
            PromotionDao promotionDao = new PromotionDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            Employee employee = employeeDao.get(reciept.getEmployeeId());
            Promotion promotion = promotionDao.get(reciept.getPromotionId());
            reciept.setCustomer(customer);
            reciept.setEmployee(employee);
            reciept.setPromotion(promotion);

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
