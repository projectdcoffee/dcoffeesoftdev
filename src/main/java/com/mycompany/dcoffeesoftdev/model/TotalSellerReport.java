/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;
import com.mycompany.dcoffeesoftdev.dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class TotalSellerReport {

    private int id;
    private int countOrders;
    private int sumTotalSeller;

    public TotalSellerReport(int id, int countOrders, int sumTotalSeller) {
        this.id = id;
        this.countOrders = countOrders;
        this.sumTotalSeller = sumTotalSeller;
    }

    public TotalSellerReport(int countOrders, int sumTotalSeller) {
        this.id = -1;
        this.countOrders = countOrders;
        this.sumTotalSeller = sumTotalSeller;

    }

    public TotalSellerReport() {
        this(1, 0, 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountOrders() {
        return countOrders;
    }

    public void setCountOrders(int countOrders) {
        this.countOrders = countOrders;
    }

    public int getSumTotalSeller() {
        return sumTotalSeller;
    }

    public void setSumTotalSeller(int sumTotalSeller) {
        this.sumTotalSeller = sumTotalSeller;
    }

    @Override
    public String toString() {
        return "TotalSellerReport{" + "id=" + id + ", countOrders=" + countOrders + ", sumTotalSeller=" + sumTotalSeller + '}';
    }

    public static TotalSellerReport fromRS(ResultSet rs) {
        TotalSellerReport totalSellerReport = new TotalSellerReport();
        try {
            totalSellerReport.setCountOrders(rs.getInt("rec_id"));
            totalSellerReport.setSumTotalSeller(rs.getInt("rec_total"));

        } catch (SQLException ex) {
            Logger.getLogger(TotalSellerReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return totalSellerReport;
    }

}
