/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.SummarySalaryDao;
import com.mycompany.dcoffeesoftdev.model.SummarySalary;
import java.util.List;

/**
 *
 * @author Love_
 */
public class SummarySalaryService {

    public SummarySalary getById(int id) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.get(id);
    }

    public List<SummarySalary> getSummarySalarys() {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.getAll(" ss_id asc");
    }

    public SummarySalary addNew(SummarySalary editedSummarySalary) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.save(editedSummarySalary);
    }

    public SummarySalary update(SummarySalary editedSummarySalary) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.update(editedSummarySalary);
    }

    public int delete(SummarySalary editedSummarySalary) {
        SummarySalaryDao summarySalaryDao = new SummarySalaryDao();
        return summarySalaryDao.delete(editedSummarySalary);
    }
}
