/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.SupplierDao;
import com.mycompany.dcoffeesoftdev.model.Supplier;
import java.util.List;

/**
 *
 * @author slmfr
 */
public class SupplierService {

    public Supplier getSupplierById(int id) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.get(id);
    }

    public List<Supplier> getSupplier() {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.getAll();
    }

    public Supplier addNew(Supplier obj) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.save(obj);
    }

    public Supplier update(Supplier obj) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.update(obj);
    }

    public int delete(Supplier obj) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.delete(obj);
    }
}
