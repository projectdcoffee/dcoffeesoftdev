/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.EmployeeDao;
import com.mycompany.dcoffeesoftdev.model.Employee;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class EmployeeService {

    public static Employee currentEmployee;

    public List<Employee> getEmployee() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll(" employee_id asc");
    }

    public Employee getEmployeeByUserId(int userId) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getByUserId(userId);
    }

    public Employee addEmployee(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmployee);
    }

    public Employee updateEmployee(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmployee);
    }

    public int deleteEmployee(Employee editedEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmployee);
    }

}

