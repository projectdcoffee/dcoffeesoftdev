/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.AddExpensesDao;
import com.mycompany.dcoffeesoftdev.dao.AddExpensesDetailDao;
import com.mycompany.dcoffeesoftdev.model.AddExpenses;
import com.mycompany.dcoffeesoftdev.model.AddExpensesDetail;
import com.mycompany.dcoffeesoftdev.model.ExpensesReport;
import java.util.List;

/**
 *
 * @author fkais
 */
public class AddExpensesService {

    public AddExpenses getById(int id) {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        return addExpensesDao.get(id);
    }
    
    public List<AddExpenses> getAddExpensess() {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        return addExpensesDao.getAll(" ae_id asc");
    }
    
    public AddExpenses addNew(AddExpenses editedAddExpenses) {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        AddExpensesDetailDao addExpensesDetailDao = new AddExpensesDetailDao();
        AddExpenses addExpenses = addExpensesDao.save(editedAddExpenses);
        for (AddExpensesDetail aed : editedAddExpenses.getAddExpensesDetail()) {
            aed.setAddExpensesId(addExpenses.getId());
            addExpensesDetailDao.save(aed);
        }
        return addExpenses;
    }
    
    public AddExpenses update(AddExpenses editedAddExpenses) {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        return addExpensesDao.update(editedAddExpenses);
    }
    
    public int delete(AddExpenses editedAddExpenses) {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        return addExpensesDao.delete(editedAddExpenses);
    }
    
    public int deleteAddExpenses(AddExpenses editedAddExpenses) {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        return addExpensesDao.delete(editedAddExpenses);
    }
    
        public List<ExpensesReport> getExpensesReports(String begin, String end) {
        AddExpensesDao addExpensesDao = new AddExpensesDao();
        return addExpensesDao.getExpensesReports(begin, end);
    }
}
