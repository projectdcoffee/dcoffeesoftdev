/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.BillDetailDao;
import com.mycompany.dcoffeesoftdev.model.BillDetail;
import java.util.List;

/**
 *
 * @author Love_
 */
public class BillDetailService {

    public List<BillDetail> getBillDetail() {
        BillDetailDao billDetail = new BillDetailDao();
        return billDetail.getAll(" bill_detail_id asc");
    }

    public BillDetail addBillDetail(BillDetail editedBillDetail) {
        BillDetailDao billDetail = new BillDetailDao();
        return billDetail.save(editedBillDetail);
    }

    public BillDetail updateBillDetail(BillDetail editedBillDetail) {
        BillDetailDao billDetail = new BillDetailDao();
        return billDetail.update(editedBillDetail);
    }

    public int deleteBillDetail(BillDetail editedBillDetail) {
        BillDetailDao billDetail = new BillDetailDao();
        return billDetail.delete(editedBillDetail);
    }
}
