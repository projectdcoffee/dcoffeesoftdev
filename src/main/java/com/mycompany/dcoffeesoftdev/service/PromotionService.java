/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.PromotionDao;
import com.mycompany.dcoffeesoftdev.model.Promotion;
import java.util.List;

/**
 *
 * @author fkais
 */
public class PromotionService {
    
    public Promotion getByCodePro(String codePro) {
        PromotionDao promotionDao = new PromotionDao();
        Promotion promotion = promotionDao.getByCodePro(codePro);
        return promotion;
    }
        public Promotion getById(int id) {
        PromotionDao promotionDao = new PromotionDao();
        Promotion promotion = promotionDao.get(id);
        return promotion;
    }
    
    public List<Promotion> getPromotions() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" pmt_id asc");
    }
    
    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }
    
    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }
    
    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
}
