/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.CheckMaterialDetailDao;
import com.mycompany.dcoffeesoftdev.model.CheckMaterialDetail;
import java.util.List;

/**
 *
 * @author hp
 */
public class CheckMaterialDetailService {

    public List<CheckMaterialDetail> getCheckMaterialDetail() {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        return checkMaterialDetailDao.getAll(" cmd_id asc");
    }
    
    public List<CheckMaterialDetail> getCheckMaterialDetailByCheckMatId(int checkMatId) {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        return checkMaterialDetailDao.getAll(" check_mat_id="+checkMatId," cmd_id asc");
    }

    public CheckMaterialDetail addCheckMaterialDetail(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        return checkMaterialDetailDao.save(editedCheckMaterialDetail);
    }

    public CheckMaterialDetail updateCheckMaterialDetail(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        return checkMaterialDetailDao.update(editedCheckMaterialDetail);
    }

    public int deleteCheckMaterialDetail(CheckMaterialDetail editedCheckMaterialDetail) {
        CheckMaterialDetailDao checkMaterialDetailDao = new CheckMaterialDetailDao();
        System.out.println("DELETE ");
        return checkMaterialDetailDao.delete(editedCheckMaterialDetail);
    }
}
