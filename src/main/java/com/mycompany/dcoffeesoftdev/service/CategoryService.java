/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.CategoryDao;
import com.mycompany.dcoffeesoftdev.model.Category;
import java.util.List;

/**
 *
 * @author hp
 */
public class CategoryService {

    public List<Category> getCategorys() {
        CategoryDao catDao = new CategoryDao();
        return catDao.getAll(" category_id asc");
    }

//    public Category addCategory(Category editedCategory) {
//        CategoryDao catDao = new CategoryDao();
//        return catDao.save(editedCategory);
//    }
//    
//     public Category updateCategory(Category editedCategory) {
//        CategoryDao catDao = new CategoryDao();
//        return catDao.update(editedCategory);
//    }
//     
//      public int  deleteCategory(Category editedCategory) {
//        CategoryDao catDao = new CategoryDao();
//        return catDao.delete(editedCategory);
//    }
    public Category addNew(Category editedCategory) {
        CategoryDao catDao = new CategoryDao();
        return catDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao catDao = new CategoryDao();
        return catDao.update(editedCategory);

    }

    public int delete(Category editedCategory) {
        CategoryDao catDao = new CategoryDao();
        return catDao.delete(editedCategory);
    }
}
