/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.StoreDao;
import com.mycompany.dcoffeesoftdev.dao.UserDao;
import com.mycompany.dcoffeesoftdev.model.Store;
import com.mycompany.dcoffeesoftdev.model.User;
import java.util.List;

/**
 *
 * @author fkais
 */
public class UserService {

    public static User currentUser;
    public static Store currentStore;
    
    public User login(String login, String password,int storeId) {
        UserDao userDao = new UserDao();
        StoreDao storeDao = new StoreDao();
        User user = userDao.getByLogin(login);
        Store store = storeDao.get(storeId);
        if (user != null && user.getPassword().equals(password) && store != null) {
            currentStore = store;
            currentUser = user;
            return user;
        }
        return null;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_id asc");
    }
    
        
     public List<User> getUsers(String where , String order) {
        UserDao userDao = new UserDao();
        return userDao.getAll(where,order);
    }
     
      public User getById(int id) {
        UserDao userDao = new UserDao();
        return userDao.get(id);
    }

    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        System.out.println(editedUser);
        return userDao.save(editedUser);
    }

    public User update(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }
}
