/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.AddExpensesDetailDao;
import com.mycompany.dcoffeesoftdev.model.AddExpensesDetail;
import java.util.List;

/**
 *
 * @author fkais
 */
public class AddExpensesDetailService {
    public AddExpensesDetail getAddExpensesDetailByID(int id) {
        AddExpensesDetailDao aedDao = new AddExpensesDetailDao();
        return aedDao.get(id);
    }
    
    public List<AddExpensesDetail>getAddExpensesDetail() {
        AddExpensesDetailDao aedDao = new AddExpensesDetailDao();
        return aedDao.getAll(" aed_id asc");
    }
    
    public List<AddExpensesDetail>getAddExpensesDetailsByAddExpensesId(int AddExpensesId) {
        AddExpensesDetailDao aedDao = new AddExpensesDetailDao();
        return aedDao.getAll(" ae_id=" + AddExpensesId," aed_id");
    }
    
    public AddExpensesDetail addAddExpensesDetail(AddExpensesDetail editedAddExpensesDetail) {
        AddExpensesDetailDao aedDao = new AddExpensesDetailDao();
        return aedDao.save(editedAddExpensesDetail);
    }
    
    public AddExpensesDetail updateAddExpensesDetail(AddExpensesDetail editedAddExpensesDetail) {
        AddExpensesDetailDao aedDao = new AddExpensesDetailDao();
        return aedDao.update(editedAddExpensesDetail);
    }
    
    public int deleteAddExpensesDetail(AddExpensesDetail editedAddExpensesDetail) {
        AddExpensesDetailDao aedDao = new AddExpensesDetailDao();
        return aedDao.delete(editedAddExpensesDetail);
    }
}
