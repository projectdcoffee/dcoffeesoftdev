/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.CheckMaterialDao;
import com.mycompany.dcoffeesoftdev.dao.CheckMaterialDetailDao;
import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import com.mycompany.dcoffeesoftdev.model.CheckMaterial;
import com.mycompany.dcoffeesoftdev.model.CheckMaterialDetail;
import com.mycompany.dcoffeesoftdev.model.Material;
import java.util.List;

/**
 *
 * @author hp
 */
public class CheckMaterialService {

    CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
    CheckMaterialDetailDao cmdDao = new CheckMaterialDetailDao();
    MaterialDao matDao = new MaterialDao();

    public List<CheckMaterial> getCheckMaterial() {
        return checkMaterialDao.getAll(" check_mat_id asc");
    }

    public CheckMaterial addCheckMaterial(CheckMaterial editedCheckMaterial) {
        CheckMaterial checkMaterial = checkMaterialDao.save(editedCheckMaterial);
        for (CheckMaterialDetail cmd : editedCheckMaterial.getCmd()) {
            cmd.setCheckMatId(checkMaterial.getId());
            cmdDao.save(cmd);
            Material material = matDao.get(cmd.getMatId());
            if (material != null) {
                material.setQty(cmd.getCmdQtyRemain()-cmd.getCmdQtyExpired());
                matDao.update(material);
            }
            
        }
        return checkMaterial;
    }

    public CheckMaterial updateCheckMaterial(CheckMaterial editedCheckMaterial) {
        return checkMaterialDao.update(editedCheckMaterial);
    }

    public int deleteCheckMaterial(CheckMaterial editedCheckMaterial) {
        System.out.println("DELETE ");
        return checkMaterialDao.delete(editedCheckMaterial);
    }
}
