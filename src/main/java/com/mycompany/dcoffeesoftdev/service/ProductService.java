/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.ProductDao;
import com.mycompany.dcoffeesoftdev.model.Product;
import com.mycompany.dcoffeesoftdev.model.ProductBestSeller5Report;
import com.mycompany.dcoffeesoftdev.model.ProductWorstSeller5Report;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Love_
 */
public class ProductService {

    private ProductDao productDao = new ProductDao();

    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name ASC");
    }

    public List<Product> getProductsByCategoryId(int categoryId) {
        return productDao.getByCategoryID(categoryId);
    }

    public List<Product> getProducts() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }

    public List<ProductBestSeller5Report> getBestSeller5Report() {
        ProductDao productDao = new ProductDao();
        return productDao.getBestSeller5Report();
    }
    
        public List<ProductWorstSeller5Report> getWorstSeller5Report() {
        ProductDao productDao = new ProductDao();
        return productDao.getWorstSeller5Report();
    }
}
