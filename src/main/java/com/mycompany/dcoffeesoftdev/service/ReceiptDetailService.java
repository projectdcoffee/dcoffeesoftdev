/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.ReceiptDetailDao;
import com.mycompany.dcoffeesoftdev.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author Love_
 */
public class ReceiptDetailService {
    public ReceiptDetail getReceiptDetailByID(int id) {
        ReceiptDetailDao rcdDao = new ReceiptDetailDao();
        return rcdDao.get(id);
    }
    
    public List<ReceiptDetail> getReceiptDetail() {
        ReceiptDetailDao rcdDao = new ReceiptDetailDao();
        return rcdDao.getAll(" rcd_id asc");
    }
    
    public List<ReceiptDetail> getReceiptDetailsByReceiptId(int ReceiprId){
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.getAll(" rec_id =" + ReceiprId, " rcd_id");
    }

    public ReceiptDetail addReceiptDetail(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao rcdDao = new ReceiptDetailDao();
        return rcdDao.save(editedReceiptDetail);
    }

    public ReceiptDetail updateReceiptDetail(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao rcdDao = new ReceiptDetailDao();
        return rcdDao.update(editedReceiptDetail);
    }
    
}