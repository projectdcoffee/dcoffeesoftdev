/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.ReceiptDao;
import com.mycompany.dcoffeesoftdev.dao.ReceiptDetailDao;
import com.mycompany.dcoffeesoftdev.model.Customer;
import com.mycompany.dcoffeesoftdev.model.Promotion;
import com.mycompany.dcoffeesoftdev.model.Receipt;
import com.mycompany.dcoffeesoftdev.model.ReceiptDetail;
import com.mycompany.dcoffeesoftdev.model.SellerGraphReport;
import com.mycompany.dcoffeesoftdev.model.TotalSellerReport;
import java.util.List;

/**
 *
 * @author fkais
 */
public class ReceiptService {
    private CustomerService customerService = new CustomerService();
    private PromotionService promotionService =new PromotionService();
    private ReceiptDao receiptDao = new ReceiptDao();
    private ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();

    public Receipt getById(int id) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.get(id);
    }

    public List<Receipt> getReceipts() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(" rec_id asc");
    }

    public Receipt addNew(Receipt editedReceipt) {
        int point = 0;
        int currentPoint  = 0;
        Receipt receipt = receiptDao.save(editedReceipt);
        Customer customer = customerService.getById(receipt.getCustomerId());
        Promotion promotion = promotionService.getById(receipt.getPromotionId());
        for (ReceiptDetail rd : editedReceipt.getReceiptDetails()) {
            point += rd.getQty();
            rd.setReceiptId(receipt.getId());
            receiptDetailDao.save(rd);
        }
        if(receipt.getCustomerId() > 0  ){
            currentPoint = customer.getPoint();
            memberDiscount(promotion, customer, currentPoint);
            updateCustomerPoint(customer, point,currentPoint);
        } 
             
        
        return receipt;
    }

    private void memberDiscount(Promotion promotion, Customer customer, int currentPoint) {
        if(promotion.getCodePro().equals("MEMBER")){
            customer.setPoint(currentPoint - 10);
        }
    }

    private void updateCustomerPoint(Customer customer,int point,int currentPoint) {
        currentPoint = customer.getPoint();
        customer.setPoint(point+currentPoint);
        customerService.update(customer);
    }

    public Receipt update(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReceipt);
    }

    public int delete(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReceipt);
    }

    public List<TotalSellerReport> getTotalSellerReport() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getTotalSellerReport();
    }

    public List<SellerGraphReport> getSellerGraphReports() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getSellerGraphReports();
    }

    public List<SellerGraphReport> getSellerGraphReports(String begin, String end) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getSellerGraphReports(begin, end);
    }

}
