/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.CheckInOutDao;
import com.mycompany.dcoffeesoftdev.model.CheckInOut;
import java.sql.Timestamp;
import static java.sql.Types.NULL;
import java.time.LocalDateTime;

import java.util.List;

/**
 *
 * @author fkais
 */
public class CheckInOutService {

    public CheckInOut getById(int id) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.get(id);
    }

    public List<CheckInOut> getByEmployeeId(int id,String where) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getByEmployeeId(id,where);
    }

    public List<CheckInOut> getCheckInOuts() {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAll(" cio_id asc");
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        editedCheckInOut.setIn(timeStampDateTimeNow());
        return checkInOutDao.save(editedCheckInOut);
    }

    public Timestamp timeStampDateTimeNow() {

        LocalDateTime datetime = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(datetime);
        return timestamp;
    }

    public CheckInOut CheckOutByEmployee(int id) {
        CheckInOutDao checkinoutDao = new CheckInOutDao();
        CheckInOut checkinout = new CheckInOut();
        checkinout = checkinoutDao.getLastCheckInOutByEmployeeId(id);
        checkinout.setOut(timeStampDateTimeNow());
        checkinout.calculateTotaHour();
        return checkinoutDao.update(checkinout);
    }

    public boolean isCheckIn(int id) {
        CheckInOutDao checkinoutDao = new CheckInOutDao();
        CheckInOut checkInOut = checkinoutDao.getLastCheckInOutByEmployeeId(id);
        if (checkInOut == null) {
            return false;
        }
        return true;
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.delete(editedCheckInOut);
    }
}
