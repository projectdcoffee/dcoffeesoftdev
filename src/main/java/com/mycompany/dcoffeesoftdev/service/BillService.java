/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.BillDao;
import com.mycompany.dcoffeesoftdev.dao.BillDetailDao;
import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import com.mycompany.dcoffeesoftdev.model.Bill;
import com.mycompany.dcoffeesoftdev.model.BillDetail;
import com.mycompany.dcoffeesoftdev.model.Material;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class BillService {

    public Bill getById(int id) {
        BillDao billDao = new BillDao();
        return billDao.get(id);
    }

    public List<Bill> getBills() {
        BillDao billDao = new BillDao();
        return billDao.getAll(" bill_id asc");
    }

    public Bill addNew(Bill editedBill) {
        System.out.println(editedBill);
        BillDao billDao = new BillDao();
        BillDetailDao billDetailDao = new BillDetailDao();
        MaterialDao materialDao = new MaterialDao();
        Bill bill = billDao.save(editedBill);
        
        
        //save one by one
        for (BillDetail bd : editedBill.getBillDetails()) {
            System.out.println(bd);
           bd.setBillId(bill.getId());
            billDetailDao.save(bd);

//          
            
            Material material = materialDao.get(bd.getMaterialId());
            int qty = material.getQty();
            material.setQty(qty+bd.getAmout());
            material.setPricePerUnit(bd.getPrice());
            materialDao.update(material);
       }
        
       
        return bill;
    }

    public Bill update(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        System.out.println(editedBill.getId());
        return billDao.delete(editedBill);
    }

}
