/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.dao;

import com.mycompany.dcoffeesoftdev.service.*;
import com.mycompany.dcoffeesoftdev.helper.DatabaseHelper;
import com.mycompany.dcoffeesoftdev.model.AddExpenses;
import com.mycompany.dcoffeesoftdev.model.AddExpensesDetail;
import com.mycompany.dcoffeesoftdev.model.ExpensesReport;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.spi.DirStateFactory;

/**
 *
 * @author fkais
 */
public class AddExpensesDao implements Dao<AddExpenses> {

    @Override
    public AddExpenses get(int id) {
        AddExpenses addExpenses = null;
        String sql = "SELECT * FROM add_expenses WHERE ae_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                addExpenses = AddExpenses.fromRs(rs);
                AddExpensesDetailDao aedd = new AddExpensesDetailDao();
                ArrayList<AddExpensesDetail> addExpensesDetails = (ArrayList<AddExpensesDetail>) aedd.getAll("ae_id=" + addExpenses.getId(), "aed_id");
                addExpenses.setAddExpensesDetail(addExpensesDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return addExpenses;
    }

    @Override
    public List<AddExpenses> getAll() {
        ArrayList<AddExpenses> list = new ArrayList();
        String sql = "SELECT * FROM add_expenses";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddExpenses addExpenses = AddExpenses.fromRs(rs);
                list.add(addExpenses);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<AddExpenses> getAll(String where, String order) {
        ArrayList<AddExpenses> list = new ArrayList();
        String sql = "SELECT * FROM add_expenses where" + where + "ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddExpenses addExpenses = AddExpenses.fromRs(rs);
                list.add(addExpenses);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<AddExpenses> getAll(String order) {
        ArrayList<AddExpenses> list = new ArrayList();
        String sql = "SELECT * FROM add_expenses  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddExpenses addExpenses = AddExpenses.fromRs(rs);
                list.add(addExpenses);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public AddExpenses save(AddExpenses obj) {
        String sql = "INSERT INTO add_expenses ( ae_total)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public AddExpenses update(AddExpenses obj) {
        String sql = "UPDATE add_expenses"
                + "SET total = ?"
                + "WHERE ae_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(AddExpenses obj) {
        String sql = "DELETE FROM add_expenses WHERE ae_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<ExpensesReport> getExpensesReports(String begin, String end) {
        ArrayList<ExpensesReport> list = new ArrayList();
        String sql = """
                        SELECT strftime('%m',DATE(ae_datetime)) as ae_date, SUM(ae_total) as ae_total
                     FROM add_expenses
                     WHERE ae_datetime BETWEEN ? AND ?
                     GROUP BY ae_date
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ExpensesReport obj = ExpensesReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
