/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.dao;

import com.mycompany.dcoffeesoftdev.helper.DatabaseHelper;
import com.mycompany.dcoffeesoftdev.model.AddExpensesDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fkais
 */
public class AddExpensesDetailDao implements Dao<AddExpensesDetail> {
   
    @Override
    public AddExpensesDetail get(int id) {
        AddExpensesDetail addExpensesDetail = null;
        String sql = "SELECT * FROM add_expenses_detail WHERE aed_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                addExpensesDetail = AddExpensesDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return addExpensesDetail;
    }

    public List<AddExpensesDetail> getAll() {
        ArrayList<AddExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM add_expenses_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddExpensesDetail addExpensesDetail = AddExpensesDetail.fromRS(rs);
                list.add(addExpensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<AddExpensesDetail> getAll(String where, String order) {
        ArrayList<AddExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM add_expenses_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddExpensesDetail addExpensesDetail = AddExpensesDetail.fromRS(rs);
                list.add(addExpensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<AddExpensesDetail> getAll(String order) {
        ArrayList<AddExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM add_expenses_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                AddExpensesDetail addExpensesDetail = AddExpensesDetail.fromRS(rs);
                list.add(addExpensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public AddExpensesDetail save(AddExpensesDetail obj) {

        String sql = "INSERT INTO add_expenses_detail (aed_name, aed_total, ae_id)" 
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getTotal());
            stmt.setInt(3, obj.getAddExpensesId());
            
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public AddExpensesDetail update(AddExpensesDetail obj) {
        String sql = "UPDATE add_expenses_detail"
                + " SET aed_name = ?, aed_total = ?, ae_id = ?"
                + " WHERE aed_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getTotal());
            stmt.setInt(3, obj.getAddExpensesId());
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(AddExpensesDetail obj) {
        String sql = "DELETE FROM add_expenses_detail WHERE aed_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
