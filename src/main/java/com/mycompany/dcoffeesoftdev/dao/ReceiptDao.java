/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.dao;

import com.mycompany.dcoffeesoftdev.helper.DatabaseHelper;
import com.mycompany.dcoffeesoftdev.model.Receipt;
import com.mycompany.dcoffeesoftdev.model.ReceiptDetail;
import com.mycompany.dcoffeesoftdev.model.SellerGraphReport;
import com.mycompany.dcoffeesoftdev.model.TotalSellerReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fkais
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        Receipt receipt = null;
        String sql = "SELECT * FROM receipt WHERE rec_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> receiptDetails = (ArrayList<ReceiptDetail>) rdd.getAll("rec_id=" + receipt.getId(), " rcd_id asc");
                receipt.setReceiptDetails(receiptDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where" + where + "ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt reciept = Receipt.fromRS(rs);
                list.add(reciept);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO receipt ( rec_total, rec_cash, rec_change, rec_payment_type, rec_totalQty, employee_id, customer_id, store_id, rec_discount, pmt_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setFloat(3, obj.getChange());
            stmt.setString(4, obj.getPaymentType());
            stmt.setInt(5, obj.getTotalQty());
            stmt.setInt(6, obj.getEmployeeId());
            stmt.setInt(7, obj.getCustomerId());
            stmt.setInt(8, obj.getStoreId());
            stmt.setFloat(9, obj.getDiscount());
            stmt.setInt(10,obj.getPromotionId());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE receipt"
                + " SET total = ?, rec_cash = ?, rec_totalQty = ?, employee_id = ?, customer_id = ?, pmt_id = ?"
                + " WHERE rec_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getEmployeeId());
            stmt.setInt(5, obj.getCustomerId());
            stmt.setInt(6,obj.getPromotionId());
            stmt.setInt(7, obj.getId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE rec_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<TotalSellerReport> getTotalSellerReport() {
        ArrayList<TotalSellerReport> list = new ArrayList();
        String sql = """
                     SELECT
                         SUM(rec_total) as rec_total,
                         COUNT(rec_id) as rec_id,
                         DATE('now') as today
                     FROM
                         receipt
                     WHERE
                         DATE(rec_datetime) = DATE('now')
                     GROUP BY
                         DATE('now');""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                TotalSellerReport obj = TotalSellerReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SellerGraphReport> getSellerGraphReports(String begin, String end) {
        ArrayList<SellerGraphReport> list = new ArrayList();
        String sql = """
                     SELECT
                         SUM(rec_total) as rec_total,
                         strftime('%Y-%m', rec_datetime) as month
                     FROM
                         receipt
                     WHERE
                         rec_datetime BETWEEN ? AND ?
                     GROUP BY
                         month
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SellerGraphReport obj = SellerGraphReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SellerGraphReport> getSellerGraphReports() {
        ArrayList<SellerGraphReport> list = new ArrayList();
        String sql = """
                     SELECT
                         SUM(rec_total) as rec_total,
                         strftime('%Y-%m', rec_datetime) as month
                     FROM
                         receipt
                     GROUP BY
                         month
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SellerGraphReport obj = SellerGraphReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
