/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.componentpos;

import com.mycompany.dcoffeesoftdev.model.Material;

/**
 *
 * @author slmfr
 */
public interface ISubscriber {
    public void Update(Material material , int qty,float price);
}
