/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.componentpos;

import com.mycompany.dcoffeesoftdev.model.Product;

/**
 *
 * @author fkais
 */
public interface BuyProductable {
    public void buy(Product product, int qty, String size ,String type );
}
