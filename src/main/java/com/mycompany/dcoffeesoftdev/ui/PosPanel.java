/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.ui;

import com.mycompany.dcoffeesoftdev.componentpos.BuyProductable;
import com.mycompany.dcoffeesoftdev.componentpos.ProductListPanel;
import com.mycompany.dcoffeesoftdev.model.Employee;
import com.mycompany.dcoffeesoftdev.model.Product;
import com.mycompany.dcoffeesoftdev.model.Receipt;
import com.mycompany.dcoffeesoftdev.model.ReceiptDetail;
import com.mycompany.dcoffeesoftdev.model.Store;
import com.mycompany.dcoffeesoftdev.model.User;
import com.mycompany.dcoffeesoftdev.service.EmployeeService;
import com.mycompany.dcoffeesoftdev.service.ProductService;
import com.mycompany.dcoffeesoftdev.service.ReceiptDetailService;
import com.mycompany.dcoffeesoftdev.service.ReceiptService;
import com.mycompany.dcoffeesoftdev.service.UserService;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author fkais
 */
public class PosPanel extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    ReceiptService receiptService = new ReceiptService();
    ReceiptDetailService rcdService = new ReceiptDetailService();
    Receipt receipt;
    private ProductListPanel productListPanel;
    private Employee editedemployee;
    private Receipt editedreceipt;
    private User user;
    private Store store;

    /**
     * Creates new form PosPanel
     */
    public PosPanel() {
        initComponents();

        setReceiptHeader();
        lblEmployeeName.setText(
                "User: "
                + UserService.getCurrentUser().getEmployee().getFname() + " "
                + UserService.getCurrentUser().getEmployee().getLname()
        );
        receipt.setEmployee(UserService.getCurrentUser().getEmployee());
        lblEmployeePosition.setText("Position: "+UserService.getCurrentUser().getEmployee().getPosition());
        initRCD();
        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }

    private void setReceiptHeader() {
        receipt = new Receipt();
        user = UserService.currentUser;
        store = UserService.currentStore;
        receipt.setEmployeeId(user.getEmployee().getId());
        receipt.setStoreId(store.getId());
    }

    private void initRCD() {
        tblReceiptDetail.setRowHeight(100);
        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Size", "Type", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getReceiptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> recieptDetails = receipt.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                return switch (columnIndex) {
                    case 0 ->
                        recieptDetail.getProName();
                    case 1 ->
                        recieptDetail.getProPrice();
                    case 2 ->
                        recieptDetail.getSize();
                    case 3 ->
                        recieptDetail.getType();
                    case 4 ->
                        recieptDetail.getQty();
                    case 5 ->
                        recieptDetail.getTotalPrice();
                    default ->
                        "";
                };
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> recieptDetails = receipt.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    receipt.calculateTotal();
                    refreshReceipt();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return switch (columnIndex) {
                    case 2 ->
                        true;
                    default ->
                        false;
                };
            }
        });
        tblReceiptDetail.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = tblReceiptDetail.rowAtPoint(e.getPoint());
                    if (row >= 0) {
                        ArrayList<ReceiptDetail> recieptDetails = receipt.getReceiptDetails();
                        ReceiptDetail recieptDetail = recieptDetails.get(row);
                        receipt.delRecieptDetail(recieptDetail);
                        refreshReceipt();
                    }
                }
            }

        });
    }

    private void refreshReceipt() {
        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();
        lblTotal.setText("Total: " + receipt.getTotal());
        lblTotalQTY.setText("Total: " + receipt.getTotalQty());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCalculate = new javax.swing.JButton();
        lblTotal = new javax.swing.JLabel();
        scrProductList = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();
        btnClear = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblEmployeeName = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblEmployeePosition = new javax.swing.JLabel();
        lblTotalQTY = new javax.swing.JLabel();

        btnCalculate.setBackground(new java.awt.Color(102, 255, 102));
        btnCalculate.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("Total: 0");

        scrProductList.setBackground(new java.awt.Color(255, 255, 255));

        tblReceiptDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblReceiptDetail);

        btnClear.setBackground(new java.awt.Color(255, 102, 153));
        btnClear.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(64, 34, 24));

        lblEmployeeName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblEmployeeName.setForeground(new java.awt.Color(255, 255, 255));
        lblEmployeeName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEmployeeName.setText("EmployeeName");

        jLabel1.setFont(new java.awt.Font("MV Boli", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Point of Sell");

        lblEmployeePosition.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblEmployeePosition.setForeground(new java.awt.Color(255, 255, 255));
        lblEmployeePosition.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEmployeePosition.setText("EmployeePosition");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblEmployeePosition)
                    .addComponent(lblEmployeeName))
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(lblEmployeeName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblEmployeePosition)))
                .addGap(15, 15, 15))
        );

        lblTotalQTY.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotalQTY.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalQTY.setText("Total QTY: 0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scrProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(87, 87, 87)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTotal)
                            .addComponent(lblTotalQTY)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalQTY)
                        .addGap(18, 18, 18)
                        .addComponent(lblTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(scrProductList))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
//        System.out.println(receipt);
        openDialog();
    }//GEN-LAST:event_btnCalculateActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clearReceipt();
    }//GEN-LAST:event_btnClearActionPerformed

    private void clearReceipt() {
        if (receipt.getId() < 1) {
            setReceiptHeader();
            refreshReceipt();
        }
        setReceiptHeader();
        refreshReceipt();
        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PosDialog posDialog = new PosDialog(frame, receipt);
        posDialog.setLocationRelativeTo(this);
        posDialog.setVisible(true);
        posDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                clearReceipt();
            }

        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnClear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblEmployeeName;
    private javax.swing.JLabel lblEmployeePosition;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotalQTY;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblReceiptDetail;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty, String size, String type) {
        receipt.addRecieptDetail(product, qty, size, type);
//        System.out.println(receipt);
//        System.out.println(receipt.getReceiptDetails());
//            System.out.println(product + " " +size + " " + " " +  type);
        refreshReceipt();
    }
}
