/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.ui;

import com.mycompany.dcoffeesoftdev.componentpos.ISubscriber;
import com.mycompany.dcoffeesoftdev.model.Bill;
import com.mycompany.dcoffeesoftdev.model.BillDetail;
import com.mycompany.dcoffeesoftdev.model.Employee;
import com.mycompany.dcoffeesoftdev.model.Material;
import com.mycompany.dcoffeesoftdev.model.Supplier;
import com.mycompany.dcoffeesoftdev.model.User;
import com.mycompany.dcoffeesoftdev.service.BillDetailService;
import com.mycompany.dcoffeesoftdev.service.BillService;
import com.mycompany.dcoffeesoftdev.service.EmployeeService;
import com.mycompany.dcoffeesoftdev.service.MaterialService;
import com.mycompany.dcoffeesoftdev.service.SupplierService;
import com.mycompany.dcoffeesoftdev.service.UserService;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author hp
 */
public class BillDialog extends javax.swing.JDialog implements ISubscriber {

    private Bill editedBill;
    private BillDetail editedBillDetail;
    private UserService useService;
    BillDetailService billDetailService = new BillDetailService();
    private final BillService billService;
    private EmployeeService employeeService;
    private List<Employee> employees;
    private SupplierService supplierService;
    private List<Supplier> suppliers;
    private MaterialService materialService;
    private User user;

    private MaterialSelectDialog materialSelectDialog;

    /**
     * Creates new form MaterialSelectDialog
     */
    public BillDialog(JFrame parent, Bill editedBill) {
        super(parent, true);
        initComponents();
        user = UserService.getCurrentUser();
        this.editedBill = editedBill;

        if (editedBill.getId() > 0) {
            btnSave.setEnabled(false);
            btnAdd.setEnabled(false);
            cmbSuppId.setEnabled(false);
            txtBuy.setEnabled(false);
            txtChange.setEnabled(false);
            txtTotal.setEnabled(false);
        }

        employeeService = new EmployeeService();
        employees = employeeService.getEmployee();
        supplierService = new SupplierService();
        suppliers = supplierService.getSupplier();
        materialService = new MaterialService();
        tblBillDetail.setModel(new AbstractTableModel() {
            String[] columnName = {
                "Material ID", "Material", "Amout", "Price", "Total "
            };

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return editedBill.getBillDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                BillDetail bill = editedBill.getBillDetails().get(rowIndex);
                return switch (columnIndex) {
                    case 0 ->
                        bill.getMaterial().getId();
                    case 1 ->
                        bill.getMaterial().getName();
                    case 2 ->
                        bill.getAmout();
                    case 3 ->
                        bill.getPrice();
                    case 4 ->
                        bill.getTotal();
                    default ->
                        "Unknown";
                };
            }
        });

        DefaultComboBoxModel<Supplier> model1 = new DefaultComboBoxModel<Supplier>();

        for (Supplier sup : suppliers) {
            model1.addElement(sup);
        }
        cmbSuppId.setModel(model1);

        cmbSuppId.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof Supplier) {
                    value = ((Supplier) value).getName();
                }
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                return this;
            }
        });

        setObjectToForm();
        billService = new BillService();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButton1 = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        labelSuppId = new javax.swing.JLabel();
        cmbSuppId = new javax.swing.JComboBox<>();
        labelTotal = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        labelBuy = new javax.swing.JLabel();
        txtBuy = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtChange = new javax.swing.JTextField();
        labelChange = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBillDetail = new javax.swing.JTable();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        lblDashboard = new javax.swing.JLabel();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(64, 34, 24));

        labelSuppId.setForeground(new java.awt.Color(255, 255, 255));
        labelSuppId.setText("SupplierID :");

        cmbSuppId.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        labelTotal.setForeground(new java.awt.Color(255, 255, 255));
        labelTotal.setText("Total :");

        txtTotal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Baht");

        labelBuy.setForeground(new java.awt.Color(255, 255, 255));
        labelBuy.setText("Buy :");

        txtBuy.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Baht");

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Baht");

        txtChange.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChangeActionPerformed(evt);
            }
        });

        labelChange.setForeground(new java.awt.Color(255, 255, 255));
        labelChange.setText("Change :");

        btnAdd.setBackground(new java.awt.Color(0, 153, 255));
        btnAdd.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnAdd.setText("Add Material");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        tblBillDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id Bill", "Material", "Total", "Buy"
            }
        ));
        jScrollPane1.setViewportView(tblBillDetail);

        btnCancel.setBackground(new java.awt.Color(255, 102, 102));
        btnCancel.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(102, 255, 102));
        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        lblDashboard.setFont(new java.awt.Font("MV Boli", 1, 36)); // NOI18N
        lblDashboard.setForeground(new java.awt.Color(255, 255, 255));
        lblDashboard.setText("Add Bill Detail");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave)
                        .addGap(5, 5, 5))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblDashboard, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(87, 87, 87)
                .addComponent(labelSuppId)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbSuppId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelChange)
                            .addComponent(labelBuy))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtChange)
                            .addComponent(txtBuy, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelTotal)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDashboard)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTotal)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(labelSuppId)
                    .addComponent(cmbSuppId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelBuy, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelChange)
                    .addComponent(txtChange, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnSave))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
//        Bill bill;
        if (!isValidData()) {
            JOptionPane.showMessageDialog(this, "Invalid data, please check your input", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (editedBill.getId() < 0) {
            setFormToObject();
            billService.addNew(editedBill);
        } else {
            setFormToObject();
            billService.update(editedBill);
        }
        this.dispose();
    }

    private boolean isValidData() {
        if (txtBuy.getText().isEmpty() || txtChange.getText().isEmpty() || txtTotal.getText().isEmpty()) {
            return false;
        }

        try {
            float buy = Float.parseFloat(txtBuy.getText());
            float change = Float.parseFloat(txtChange.getText());
            float total = Float.parseFloat(txtTotal.getText());
            if (buy == 0 || change == 0 || total == 0) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        
        return true;
    }//GEN-LAST:event_btnSaveActionPerformed

    private void txtChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChangeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChangeActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        JDialog frame = (JDialog) SwingUtilities.getRoot(this);
        materialSelectDialog = new MaterialSelectDialog(frame);
        materialSelectDialog.setSubscriber(this);
        materialSelectDialog.setLocationRelativeTo(this);
        materialSelectDialog.setVisible(true);
        materialSelectDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {

            }

        });

    }//GEN-LAST:event_btnAddActionPerformed

    private void refreshTable() {
//        list = billDetailService.getBillDetail();
        tblBillDetail.revalidate();
        tblBillDetail.repaint();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox<Supplier> cmbSuppId;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelBuy;
    private javax.swing.JLabel labelChange;
    private javax.swing.JLabel labelSuppId;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel lblDashboard;
    private javax.swing.JTable tblBillDetail;
    private javax.swing.JTextField txtBuy;
    private javax.swing.JTextField txtChange;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private void setFormToObject() {

        editedBill.setEmployeeId(user.getEmployee().getId());
        Supplier supplier = new Supplier();
        supplier = (Supplier) cmbSuppId.getSelectedItem();
        editedBill.setSupplierId(supplier.getId());
        editedBill.setTotal(Float.parseFloat(txtTotal.getText()));
        editedBill.setBuy(Float.parseFloat(txtBuy.getText()));
        editedBill.setChange(Float.parseFloat(txtChange.getText()));
    }

    private void setObjectToForm() {
        cmbSuppId.setSelectedItem(editedBill.getSupplier());
        txtTotal.setText(Float.toString(editedBill.getTotal()));
        txtBuy.setText(Float.toString(editedBill.getBuy()));
        txtChange.setText(Float.toString(editedBill.getChange()));

    }

    @Override
    public void Update(Material material, int qty, float price) {

        editedBillDetail = new BillDetail();
        editedBillDetail.setMaterial(material);
        editedBillDetail.setAmout(qty);
        editedBillDetail.setName(material.getName());
        editedBillDetail.setMaterialId(material.getId());
        editedBillDetail.setPrice(price);
        editedBillDetail.setTotal(price * qty);
//        list.add(editedBillDetail);
        editedBill.addBillDetail(editedBillDetail);
        System.out.println(editedBill.getTotal());
        txtTotal.setText(editedBill.getTotal() + "");
//        System.out.println(list);

        refreshTable();
    }

}
