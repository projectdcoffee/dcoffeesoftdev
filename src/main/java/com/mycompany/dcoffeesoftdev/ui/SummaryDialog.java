/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.ui;

import com.mycompany.dcoffeesoftdev.model.CheckInOut;

import com.mycompany.dcoffeesoftdev.model.Employee;
import com.mycompany.dcoffeesoftdev.model.SummarySalary;
import com.mycompany.dcoffeesoftdev.service.CheckInOutService;

import com.mycompany.dcoffeesoftdev.service.EmployeeService;
import com.mycompany.dcoffeesoftdev.service.SummarySalaryService;
import java.awt.Component;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Love_
 */
public class SummaryDialog extends javax.swing.JDialog {

    private SummarySalary editedSummary = new SummarySalary();
    private final SummarySalaryService summaryService;
    private CheckInOutService checkInOutService = new CheckInOutService();
    private EmployeeService employeeService;
    private List<Employee> employees;
    private Employee employee  = new Employee();
    private List<CheckInOut> checkInOuts ;
    private float workhour ;
    private float summarySalary ;
    
    
    


    public SummaryDialog(java.awt.Frame parent, SummarySalary editedSummary) {
        super(parent, true);
        initComponents();
        employeeService = new EmployeeService();
        employees = employeeService.getEmployee();
        this.editedSummary = editedSummary;
        lblSalry.setText("Salary to pain : "+this.editedSummary.getSalary()+"");
        lblWorkHour.setText("Work Hour : "+this.editedSummary.getWorkhour()+"");
        cbEmployee.setSelectedItem(this.editedSummary.getEmployee());
        if(this.editedSummary.getId() > 0){
            btnSave.setEnabled(false);
            btnSelect.setEnabled(false);
            cbEmployee.setEnabled(false);
        }
        checkInOuts = checkInOutService.getByEmployeeId(this.editedSummary.getEmId(), "AND ss_id ="+editedSummary.getId());

        

        DefaultComboBoxModel<Employee> model = new DefaultComboBoxModel<Employee>();
        
        for(Employee ep : employees){
            model.addElement(ep);
        }
        cbEmployee.setModel(model);
        
        summaryService = new SummarySalaryService();
//        cbEmployee.setModel(model);
       cbEmployee.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof  Employee) {
                    value = ((Employee) value).getFname() +" "+ ((Employee) value).getLname();
                }
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                return this;
            }
        });
        TblCheckIO.setModel(new AbstractTableModel() {
            String[] headers ={"ID","DATE","Check In","Check Out","Total Hour"};
            @Override
            public String getColumnName(int column) {
                return headers[column];
            }
           
            @Override
            public int getRowCount() {
                return checkInOuts.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckInOut cio = checkInOuts.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        
                        return  cio.getId();
                        
                    case 1:
                        
                        return  cio.getDate();
                        
                    case 2:
                        return cio.getIn();
                    case 3:
                        return  cio.getOut();
                    case 4:
                        
                        return  cio.getTotalHour();
                    default:
                        return "";
                }
            }
        });
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cbEmployee = new javax.swing.JComboBox<>();
        btnSelect = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblCheckIO = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        lblWorkHour = new javax.swing.JLabel();
        lblSalry = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(67, 53, 32));

        cbEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbEmployeeActionPerformed(evt);
            }
        });

        btnSelect.setText("Select");
        btnSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectActionPerformed(evt);
            }
        });

        TblCheckIO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(TblCheckIO);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cbEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSelect)
                        .addGap(317, 317, 317))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(9, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSelect))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(227, 209, 138));

        btnSave.setBackground(new java.awt.Color(102, 204, 255));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 102, 102));
        btnClear.setText("Cancel");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        lblWorkHour.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        lblWorkHour.setText("Work Hour :");

        lblSalry.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        lblSalry.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSalry.setText(" Salary to pain :");

        jLabel1.setFont(new java.awt.Font("MV Boli", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("- Summary Salary -");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblWorkHour, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(lblSalry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblWorkHour)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSalry)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        
        editedSummary.setEmId(employee.getId());
        
        editedSummary.setSalary(summarySalary);
        editedSummary.setWorkhour(workhour);
        SummarySalary res  = summaryService.addNew(editedSummary);
        for(CheckInOut cio : checkInOuts){
            cio.setSsId(res.getId());
            checkInOutService.update(cio);
        }
        this.dispose();
        
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed

        dispose();
    }//GEN-LAST:event_btnClearActionPerformed

    private void cbEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbEmployeeActionPerformed

        
    }//GEN-LAST:event_cbEmployeeActionPerformed

    private void btnSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectActionPerformed
        employee = (Employee) cbEmployee.getSelectedItem();
        checkInOuts = checkInOutService.getByEmployeeId(employee.getId(),"AND ss_id <= 0 AND cio_out NOTNULL");
        refreshTable();
        
        
    }//GEN-LAST:event_btnSelectActionPerformed
    private void refreshTable() {
        checkInOuts = checkInOutService.getByEmployeeId(employee.getId(),"AND ss_id <= 0 AND cio_out NOTNULL");
        TblCheckIO.revalidate();
        TblCheckIO.repaint();
        calculateWorkHour();
    }
    
    private void calculateWorkHour(){
        workhour = 0;
        for(CheckInOut cio : checkInOuts){
            workhour += cio.getTotalHour();
        }
        lblWorkHour.setText("Work Hour : "+ workhour+"");
        summarySalary = workhour * employee.getWorkHourlyWage();
        lblSalry.setText("Salary to pain : "+summarySalary);
        
    
    }


      
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TblCheckIO;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSelect;
    private javax.swing.JComboBox<Employee> cbEmployee;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblSalry;
    private javax.swing.JLabel lblWorkHour;
    // End of variables declaration//GEN-END:variables
}
