/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.ui;

import com.mycompany.dcoffeesoftdev.model.AddExpenses;
import com.mycompany.dcoffeesoftdev.model.ExpensesReport;
import com.mycompany.dcoffeesoftdev.model.MatLowInStockReport;
import com.mycompany.dcoffeesoftdev.model.Product;
import com.mycompany.dcoffeesoftdev.model.ProductBestSeller5Report;
import com.mycompany.dcoffeesoftdev.model.ProductWorstSeller5Report;
import com.mycompany.dcoffeesoftdev.model.SellerGraphReport;
import com.mycompany.dcoffeesoftdev.model.TotalSellerReport;
import com.mycompany.dcoffeesoftdev.service.AddExpensesService;
import com.mycompany.dcoffeesoftdev.service.MaterialService;
import com.mycompany.dcoffeesoftdev.service.ProductService;
import com.mycompany.dcoffeesoftdev.service.ReceiptService;
import java.awt.Color;
import java.awt.Image;
import java.text.SimpleDateFormat;
import static java.util.Collections.list;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author asus
 */
public class DashboardPanel extends javax.swing.JPanel {

    private UtilDateModel model1;
    private UtilDateModel model2;
    private final MaterialService materialService;
    private final List<MatLowInStockReport> materialList;
    private AbstractTableModel modelMatLow1, modelBestSeller, modelWorstSeller;
    private final ReceiptService receiptService;
    private final List<TotalSellerReport> tsrList;
    private final ProductService productService;
    private final List<ProductBestSeller5Report> productList;
    private final List<ProductWorstSeller5Report> productList2;
    private List<SellerGraphReport> receiptList;
    private DefaultCategoryDataset barDataset1;
    private List<SellerGraphReport> sellerTotalList;
    private AddExpensesService addExpensesService;
    private List<ExpensesReport> expensesReportList;
    private DefaultCategoryDataset barDataset2;

    /**
     * Creates new form DashboardPanel
     */
    public DashboardPanel() {
        initComponents();
        //กราฟ1
        receiptService = new ReceiptService();
        tsrList = receiptService.getTotalSellerReport();
        //กราฟ2
        addExpensesService = new AddExpensesService();
        //initBarChart1();
        //initBarChart2();

        ImageIcon iconLogin = new ImageIcon("./shoppingIcon.png");
        lblLogo.setIcon(iconLogin);

        System.out.println(receiptList);
        System.out.println(tsrList);
        for (TotalSellerReport tsr : tsrList) {
            lblCountOrders1.setText("" + tsr.getCountOrders() + " orders");
            lblSumTotalReciept1.setText("" + tsr.getSumTotalSeller() + " baht");
        }

        materialService = new MaterialService();
        materialList = materialService.getMatLowReportList();
        modelMatLow1 = new AbstractTableModel() {
            String[] colNames = {"mat_id", "mat_name", "mat_Qty"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return materialList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MatLowInStockReport material = materialList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getMatId();
                    case 1:
                        return material.getMatName();
                    case 2:
                        return material.getMatQty();

                    default:
                        return "";
                }
            }

        };

        tblLowMat.setModel(modelMatLow1);

        productService = new ProductService();
        productList = productService.getBestSeller5Report();
        modelBestSeller = new AbstractTableModel() {
            String[] colNames = {"image", "product_name", "Qty."};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return productList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ProductBestSeller5Report productBestSeller = productList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./product" + productBestSeller.getProductId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;

                    case 1:
                        return productBestSeller.getProductName();
                    case 2:
                        return productBestSeller.getProductQty();

                    default:
                        return "";
                }
            }

        };
        int rowHeight = 60; // ความสูงที่คุณต้องการ
        tblBestSeller5.setRowHeight(rowHeight);
        tblBestSeller5.setModel(modelBestSeller);

        /////////
        //productService = new ProductService();
        productList2 = productService.getWorstSeller5Report();
        modelWorstSeller = new AbstractTableModel() {
            String[] colNames = {"image", "product_name", "Qty."};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return productList2.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ProductWorstSeller5Report productWorstSeller = productList2.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./product" + productWorstSeller.getProductId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;

                    case 1:
                        return productWorstSeller.getProductName();
                    case 2:
                        return productWorstSeller.getProductQty();

                    default:
                        return "";
                }
            }

        };
        tblLowSeller5.setRowHeight(rowHeight);
        tblLowSeller5.setModel(modelWorstSeller);

        initDatePicker();
    }

    private void initDatePicker() {
        //DatePicker1
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put(
                "text.today", "Today");
        p1.put(
                "text.month", "Month");
        p1.put(
                "text.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());

        pnlDatePicker1.add(datePicker1);

        model1.setSelected(
                true);

        //DatePicker2
        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put(
                "text.today", "Today");
        p2.put(
                "text.month", "Month");
        p2.put(
                "text.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(
                true);
    }

    private void initBarChart1() {
        barDataset1 = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                "Monthly Seller",
                "เดือน",
                "จำนวนเงิน(บาท)",
                barDataset1,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlShowReport.add(chartPanel);
    }

    private void loadBarDataset1() {
        barDataset1.clear();
        for (SellerGraphReport a : sellerTotalList) {
            barDataset1.setValue(a.getSumTotal(), "Total seller", a.getMonth());
        }
        System.out.println("show");

    }

    private void initBarChart2() {
        barDataset2 = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                "ค่าใช้จ่ายรายเดือน",
                "เดือน",
                "จำนวนเงิน(บาท)",
                barDataset2,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        pnlShowReport.add(chartPanel);
    }

    private void loadBarDataset2() {
        barDataset2.clear();
        for (ExpensesReport a : expensesReportList) {
            barDataset2.setValue(a.getAeTotal(), "Total", a.getAeDatetime());
        }
        System.out.println("show");

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblDashboard = new javax.swing.JLabel();
        pnlDatepicker = new javax.swing.JPanel();
        pnlDatePicker1 = new javax.swing.JPanel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnProcess = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        pnlTotal = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lblLogo = new javax.swing.JLabel();
        lblCountOrders1 = new javax.swing.JLabel();
        lblSumTotalReciept1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblLowMat = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblBestSeller5 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblLowSeller5 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnSellerTotal = new javax.swing.JButton();
        btnExpensesTotal = new javax.swing.JButton();
        pnlShowReport = new javax.swing.JPanel();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(64, 34, 24));
        jPanel1.setForeground(new java.awt.Color(102, 51, 0));

        lblDashboard.setFont(new java.awt.Font("MV Boli", 1, 36)); // NOI18N
        lblDashboard.setForeground(new java.awt.Color(255, 255, 255));
        lblDashboard.setText("Dashboard");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblDashboard, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(lblDashboard)
                .addContainerGap(11, Short.MAX_VALUE))
        );

        pnlDatepicker.setBackground(new java.awt.Color(255, 243, 228));

        pnlDatePicker1.setBackground(new java.awt.Color(255, 243, 228));

        pnlDatePicker2.setBackground(new java.awt.Color(255, 243, 228));

        btnProcess.setBackground(new java.awt.Color(255, 192, 105));
        btnProcess.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(72, 52, 52));
        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Nirmala UI", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(72, 52, 52));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("StartDate:");

        jLabel2.setFont(new java.awt.Font("Nirmala UI", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(72, 52, 52));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("EndDate:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("เลือกวันที่ต้องการ");

        javax.swing.GroupLayout pnlDatepickerLayout = new javax.swing.GroupLayout(pnlDatepicker);
        pnlDatepicker.setLayout(pnlDatepickerLayout);
        pnlDatepickerLayout.setHorizontalGroup(
            pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatepickerLayout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(134, 134, 134))
        );
        pnlDatepickerLayout.setVerticalGroup(
            pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatepickerLayout.createSequentialGroup()
                .addGroup(pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDatepickerLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel1))
                    .addGroup(pnlDatepickerLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(pnlDatepickerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pnlDatePicker1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlDatePicker2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE))))))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pnlTotal.setBackground(new java.awt.Color(241, 202, 137));

        jLabel5.setFont(new java.awt.Font("Nirmala UI", 1, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Total sales today:");

        lblLogo.setBackground(new java.awt.Color(241, 202, 137));
        lblLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLogo.setOpaque(true);

        lblCountOrders1.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        lblCountOrders1.setForeground(new java.awt.Color(72, 52, 52));
        lblCountOrders1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblCountOrders1.setText("0       orders");

        lblSumTotalReciept1.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 24)); // NOI18N
        lblSumTotalReciept1.setForeground(new java.awt.Color(72, 52, 52));
        lblSumTotalReciept1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblSumTotalReciept1.setText("0      baht");

        javax.swing.GroupLayout pnlTotalLayout = new javax.swing.GroupLayout(pnlTotal);
        pnlTotal.setLayout(pnlTotalLayout);
        pnlTotalLayout.setHorizontalGroup(
            pnlTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTotalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlTotalLayout.createSequentialGroup()
                        .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblCountOrders1, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(lblSumTotalReciept1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58))
        );
        pnlTotalLayout.setVerticalGroup(
            pnlTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTotalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlTotalLayout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblCountOrders1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSumTotalReciept1)
                .addGap(403, 403, 403))
        );

        tblLowMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblLowMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblLowMat);

        jLabel3.setBackground(new java.awt.Color(134, 84, 57));
        jLabel3.setFont(new java.awt.Font("MS Reference Sans Serif", 1, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Low Material in stock !!!");
        jLabel3.setOpaque(true);

        tblBestSeller5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblBestSeller5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblBestSeller5);

        tblLowSeller5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblLowSeller5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblLowSeller5);

        jLabel4.setBackground(new java.awt.Color(153, 255, 51));
        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Top 5 best seller");
        jLabel4.setOpaque(true);

        jLabel6.setBackground(new java.awt.Color(205, 24, 24));
        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Top 5 worst seller");
        jLabel6.setOpaque(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        btnSellerTotal.setBackground(new java.awt.Color(0, 169, 255));
        btnSellerTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnSellerTotal.setForeground(new java.awt.Color(255, 255, 255));
        btnSellerTotal.setText("แสดงกราฟยอดขายราย-เดือน");
        btnSellerTotal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSellerTotalMouseClicked(evt);
            }
        });
        btnSellerTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSellerTotalActionPerformed(evt);
            }
        });

        btnExpensesTotal.setBackground(new java.awt.Color(248, 117, 170));
        btnExpensesTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnExpensesTotal.setForeground(new java.awt.Color(255, 255, 255));
        btnExpensesTotal.setText("แสดงค่าใช้จ่ายราย-เดือน");
        btnExpensesTotal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExpensesTotalMouseClicked(evt);
            }
        });
        btnExpensesTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpensesTotalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(btnSellerTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExpensesTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(70, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnExpensesTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSellerTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE))
                .addContainerGap())
        );

        pnlShowReport.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatepicker, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlShowReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDatepicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlShowReport, javax.swing.GroupLayout.PREFERRED_SIZE, 501, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addGap(15, 15, 15))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        sellerTotalList = receiptService.getSellerGraphReports(begin, end);
        expensesReportList = addExpensesService.getExpensesReports(begin, end);
        System.out.println(sellerTotalList);
        System.out.println(expensesReportList);


    }//GEN-LAST:event_btnProcessActionPerformed

    private void btnSellerTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSellerTotalActionPerformed
        //scpGraphShow.setViewportView(new GraphMonthlySalesPanel());
        pnlShowReport.removeAll();
        pnlShowReport.revalidate();
        pnlShowReport.repaint();
        initBarChart1();
        loadBarDataset1();

    }//GEN-LAST:event_btnSellerTotalActionPerformed

    private void btnSellerTotalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSellerTotalMouseClicked

    }//GEN-LAST:event_btnSellerTotalMouseClicked

    private void btnExpensesTotalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExpensesTotalMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnExpensesTotalMouseClicked

    private void btnExpensesTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpensesTotalActionPerformed
        pnlShowReport.removeAll();
        pnlShowReport.revalidate();
        pnlShowReport.repaint();
        initBarChart2();
        loadBarDataset2();
    }//GEN-LAST:event_btnExpensesTotalActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExpensesTotal;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnSellerTotal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblCountOrders1;
    private javax.swing.JLabel lblDashboard;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblSumTotalReciept1;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JPanel pnlDatepicker;
    private javax.swing.JPanel pnlShowReport;
    private javax.swing.JPanel pnlTotal;
    private javax.swing.JTable tblBestSeller5;
    private javax.swing.JTable tblLowMat;
    private javax.swing.JTable tblLowSeller5;
    // End of variables declaration//GEN-END:variables
}
