/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.ui;

import com.mycompany.dcoffeesoftdev.model.CheckMaterial;
import com.mycompany.dcoffeesoftdev.model.CheckMaterialDetail;
import com.mycompany.dcoffeesoftdev.model.Material;
import com.mycompany.dcoffeesoftdev.model.User;
import com.mycompany.dcoffeesoftdev.service.CheckMaterialService;
import com.mycompany.dcoffeesoftdev.service.MaterialService;
import com.mycompany.dcoffeesoftdev.service.UserService;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author EliteCorps
 */
public class AddCheckMatDetailDialog extends javax.swing.JDialog {
    private CheckMaterial editedCheckMaterial;
    private List<Material> materials;
    private Material mat;
    private MaterialService materialService = new MaterialService();
    private CheckMaterialService cmService = new CheckMaterialService();
    
    private User user;
    /**
     * Creates new form CMDDialog
     */
    public AddCheckMatDetailDialog(java.awt.Frame parent, CheckMaterial editedCheckMaterial) {
        super(parent, true);
        initComponents();

        user = UserService.getCurrentUser();
        this.editedCheckMaterial = editedCheckMaterial;
        
        System.out.println(editedCheckMaterial.getId());
        materials = materialService.getMaterials();
        setMatTable();
        setCMDTable();
    }
    private void setMatTable() {
        tblMatList.setModel(new AbstractTableModel() {
            String[] columnNames = {"Material ID", "Material name", "Min QTY", "QTY", "Material Price", "Material Unit"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return materials.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material obj = materials.get(rowIndex);
                return switch (columnIndex) {
                    case 0 ->
                        obj.getId();
                    case 1 ->
                        obj.getName();
                    case 2 ->
                        obj.getMinQty();
                    case 3 ->
                        obj.getQty();
                    case 4 ->
                        obj.getPricePerUnit();
                    case 5 ->
                        obj.getUnit();    
                    default ->
                        "Unknown";
                };
            }
        });
        tblMatList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblMatList.rowAtPoint(e.getPoint());
                Material material = materials.get(row);
                editedCheckMaterial.addCMD(material);
                refreshCMD();
            }
            
        });
     }

    private void refreshCMD() {
        tblCMDList.revalidate();
        tblCMDList.repaint();
    }

     private void setCMDTable() {
        tblCMDList.setModel(new AbstractTableModel() {
            String[] columnNames = {"Mat. ID", "Mat. name", "Qty. Last", "Qty. Remain", "Qty. Expired"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return editedCheckMaterial.getCmd().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckMaterialDetail cmd = editedCheckMaterial.getCmd().get(rowIndex);
                return switch (columnIndex) {
                    case 0 ->
                        cmd.getMatId();
                    case 1 ->
                        cmd.getCmdName();
                    case 2 ->
                        cmd.getCmdQtyLast();
                    case 3 ->
                        cmd.getCmdQtyRemain();
                    case 4 ->
                        cmd.getCmdQtyExpired();
                    default ->
                        "Unknown";
                };
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<CheckMaterialDetail> cmd = editedCheckMaterial.getCmd();
                CheckMaterialDetail checkMD = cmd.get(rowIndex);
                if (columnIndex == 3) {
                    int remainQty = Integer.parseInt((String)aValue);
                    if (remainQty < 1 || remainQty > checkMD.getCmdQtyLast()) return;
                    checkMD.setCmdQtyRemain(remainQty);
                    refreshCMD();
                } else if (columnIndex == 4) {
                    int expiredQty = Integer.parseInt((String)aValue);
                    if (expiredQty < 1 || expiredQty > checkMD.getCmdQtyLast()) return;
                    checkMD.setCmdQtyExpired(expiredQty);
                    refreshCMD();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return switch (columnIndex) {
                    case 3 -> true;
                    case 4 -> true;
                    default -> false;
                };
            }
            
            
        });
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblMatList = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCMDList = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(64, 34, 24));

        jPanel1.setBackground(new java.awt.Color(64, 34, 24));

        jLabel1.setFont(new java.awt.Font("MV Boli", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Material List");

        tblMatList.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblMatList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblMatList);

        jLabel2.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Check Material Detail List");

        tblCMDList.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblCMDList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCMDList);

        btnSave.setBackground(new java.awt.Color(153, 255, 153));
        btnSave.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 153, 153));
        btnClear.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnClear, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSave, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 625, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        clearCMD();
    }//GEN-LAST:event_btnClearActionPerformed

    private void clearCMD() {
        editedCheckMaterial = new CheckMaterial();
        refreshCMD();
    }

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        System.out.println(""+editedCheckMaterial.getCmd());
        editedCheckMaterial.setEmployeeId(user.getEmployee().getId());
        cmService.addCheckMaterial(editedCheckMaterial);
        dispose();
    }//GEN-LAST:event_btnSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCMDList;
    private javax.swing.JTable tblMatList;
    // End of variables declaration//GEN-END:variables
}
